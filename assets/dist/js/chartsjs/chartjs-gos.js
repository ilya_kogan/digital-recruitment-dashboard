$(document).ready(function () {
    //polar chart 1
    var ctx = document.getElementById("polarChart");
    var myChart = new Chart(ctx, {
        type: 'polarArea',
        data: {
            datasets: [{
                data: [56, 33, 32, 64, 27, 19, 86, 84, 32, 46, 25, 22, 25, 67, 98, 46, 87, 78, 76, 69, 74, 35, 73, 67, 46, 54, 84, 74, 36, 68, 98],
                backgroundColor: [
                    "rgba(90,120,123,0.9)",
                    "rgba(80,10,123,0.8)",
                    "rgba(70,220,123,0.7)",
                    "rgba(60,0,0,0.2)",
                    "rgba(87,120,44,0.5)",
                    "rgba(17,55,123,0.5)",
                    "rgba(87,180,102,0.5)",
                    "rgba(67,120,123,0.5)",
                    "rgba(87,33,245,0.5)",
                    "rgba(134,20,2,0.5)",
                    "rgba(87,245,13,0.5)",
                    "rgba(87,120,55,0.5)",
                    "rgba(87,10,123,0.5)",
                    "rgba(7,120,36,0.5)",
                    "rgba(56,245,123,0.5)",
                    "rgba(87,10,113,0.5)",
                    "rgba(11,32,123,0.5)",
                    "rgba(87,124,223,0.5)",
                    "rgba(2,12,11,0.5)",
                    "rgba(87,22,312,0.5)",
                    "rgba(33,120,123,0.5)",
                    "rgba(56,32,123,0.5)",
                    "rgba(223,124,223,0.5)",
                    "rgba(123,12,11,0.5)",
                    "rgba(99,22,312,0.5)",
                    "rgba(34,22,312,0.5)",
                    "rgba(177,22,312,0.5)",
                    "rgba(45,22,312,0.5)",
                    "rgba(135,22,312,0.5)",
                    "rgba(54,22,312,0.5)",
                    "rgba(98,22,312,0.5)"
                ]
            }],
            labels: ["Основы программирования: типы и структуры данных", "Паттерны проектир. и основные. подходы к арх. систем.", "Теория алгоритмов", "Теория машинного обучения", "Теория облачных технологий", "Теория Blockchain/распределенный реестр", "Теория защиты информации", "Криптография", "Java", "C++", "C#", "SQL", "Python", "Scala", "R", "Go (Golang)", "Ruby", ".Net", "1С", "PHP", "JavaScript", "HTML CSS", "Swift", "Visual Basic", "Django", "jQuery", "React", "Мобильная разработка iOs", "Мобильная разработка Android", "Unity 3D", "Unreal Engine"]
        },
        options: {
            legend: {
                display: false
            },
            responsive: true
        }
    });

    //polar chart 2
    var ctx = document.getElementById("polarChart_2");
    var myChart = new Chart(ctx, {
        type: 'polarArea',
        data: {
            datasets: [{
                data: [25, 67, 25, 67, 98, 46, 87],
                backgroundColor: [
                    "rgba(60,0,0,0.2)",
                    "rgba(87,120,44,0.5)",
                    "rgba(17,55,123,0.5)",
                    "rgba(0,0,0,0.2)",
                    "rgba(67,120,123,0.5)",
                    "rgba(87,33,245,0.5)",
                    "rgba(17, 20, 123, 0.5)"
                ]
            }],
            labels: ["Разработка UX/UI", "Проектирование и дизайн мобильных интерфейсов", "Проектирование и дизайн веб-интерфейсов", "Инфографика", "Adobe Photoshop/ Adobe Illustrator", "Sketch", "Figma"]
        },
        options: {
            legend: {
                display: false
            },
            responsive: true
        }
    });

    //polar chart 3
    var ctx = document.getElementById("polarChart_3");
    var myChart = new Chart(ctx, {
        type: 'polarArea',
        data: {
            datasets: [{
                data: [67, 98, 46, 87, 25, 67, 25, 78, 54, 43, 77, 86],
                backgroundColor: [
                    "rgba(90,120,123,0.9)",
                    "rgba(80,10,123,0.8)",
                    "rgba(70,220,123,0.7)",
                    "rgba(60,0,0,0.2)",
                    "rgba(87,120,44,0.5)",
                    "rgba(17,55,123,0.5)",
                    "rgba(87,180,102,0.5)",
                    "rgba(67,120,123,0.5)",
                    "rgba(60,0,70,0.2)",
                    "rgba(87,12,44,0.5)",
                    "rgba(17,55,12,0.5)"
                ]
            }],            
            labels: ["Методы моделирования и аналитика бизнес-процессов", "Product Management", "Project Management", "Гибкие методологии управления проектами", "Юридическая и нормативная база в ИТ", "Финансовое планирование", "Управление инвестициями", "Управления финансовыми рисками", "Бренд-стратегия", "Контекстная реклама", "Мобильный маркетинг", "Трейд-маркетинг"]
        },
        options: {
            legend: {
                display: false
            },
            responsive: true
        }
    });

    //line chart    
    var ctx = document.getElementById("lineChart");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль"],
            datasets: [
                {
                    label: "01.01.2019",
                    borderColor: "rgba(0,0,0,0.9)",
                    borderWidth: "1",
                    backgroundColor: "rgba(0,0,0,0.7)",
                    data: [22, 60, 67, 43, 12, 45, 44],
                    fill: false
                },
                {
                    label: "01.06.2019",
                    borderColor: "rgba(87, 120, 123, 0.8)",
                    borderWidth: "1",
                    backgroundColor: "rgba(87, 120, 123, 0.5)",
                    pointHighlightStroke: "rgba(26,179,148,1)",
                    data: [44, 33, 50, 26, 42, 33, 12],
                    fill: false
                }
            ]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Описываем соотношение спроса и предложения: вакансии cdo / предложение на рынке cdo'
            },
            tooltips: {
                mode: 'index',
                intersect: false
            },
            hover: {
                mode: 'nearest',
                intersect: true
            }

        }
    });

    //line chart 2
    var ctx = document.getElementById("lineChart_2");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль"],
            datasets: [{
                label: 'Unfilled',
                fill: false,
                backgroundColor: '#36a2eb',
                borderColor: '#36a2eb',
                data: [22, 60, 67, 43, 12, 45, 44],
            }, {
                label: 'Dashed',
                fill: false,
                backgroundColor: '#50c1c1',
                borderColor:'#50c1c1',
                borderDash: [5, 5],
                data: [44, 33, 50, 26, 42, 33, 12],
            }, {
                label: 'Filled',
                backgroundColor: '#ff6384',
                borderColor: '#ff6384',
                data: [55, 54, 43, 32, 67, 78, 65],
                fill: true,
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Прогноз предложения'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            }
        }
    });   
});