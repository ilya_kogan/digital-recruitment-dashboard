$(document).ready(function () {

    //Animations along lines
    /**
     * SVG path for target icon
     */
    var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";

    /**
     * SVG path for plane icon
     */
    var planeSVG = "m2,106h28l24,30h72l-44,-133h35l80,132h98c21,0 21,34 0,34l-98,0 -80,134h-35l43,-133h-71l-24,30h-28l15,-47";

    /**
     * Create the map
     */
    var map = AmCharts.makeChart("amchartMap3", {
        "type": "map",
        "theme": "light",

        "projection": "winkel3",
        "dataProvider": {
            "map": "worldLow",

            "lines": [
                {
                    "id": "line1",
                    "arc": -0.85,
                    "alpha": 0.3,
                    "latitudes": [55.0364311, 55.75222],
                    "longitudes": [73.2748456, 37.61556]
                },
                {
                    "id": "line2",
                    "alpha": 0,
                    "color": "#E5343D",
                    "latitudes": [55.0364311, 55.75222],
                    "longitudes": [73.2748456, 37.61556]
                },
                {
                    "id": "line3",
                    "arc": -0.85,
                    "alpha": 0.3,
                    "latitudes": [55.0364311, 59.9386300],
                    "longitudes": [73.2748456, 30.3141300]
                },
                {
                    "id": "line4",
                    "alpha": 0,
                    "color": "#E5343D",
                    "latitudes": [55.0364311, 59.9386300],
                    "longitudes": [73.2748456, 30.3141300]
                },
                {
                    "id": "line5_kazan",
                    "arc": -0.85,
                    "alpha": 0.3,
                    "latitudes": [55.0364311, 55.7887400],
                    "longitudes": [73.2748456, 49.1221400]
                },
                {
                    "id": "line6_kazan",
                    "alpha": 0,
                    "color": "#E5343D",
                    "latitudes": [55.0364311, 55.7887400],
                    "longitudes": [73.2748456, 49.1221400]
                },
                {
                    "id": "line7_kaliningrad",
                    "arc": -0.85,
                    "alpha": 0.3,
                    "latitudes": [55.0364311, 54.707390],
                    "longitudes": [73.2748456, 20.507307]
                },
                {
                    "id": "line8_kaliningrad",
                    "alpha": 0,
                    "color": "#E5343D",
                    "latitudes": [55.0364311, 54.707390],
                    "longitudes": [73.2748456, 20.507307]
                },
                {
                    "id": "line9_ekb",
                    "arc": -0.85,
                    "alpha": 0.3,
                    "latitudes": [55.0364311, 56.838607],
                    "longitudes": [73.2748456, 60.605514]
                },
                {
                    "id": "line10_ekb",
                    "alpha": 0,
                    "color": "#E5343D",
                    "latitudes": [55.0364311, 56.838607],
                    "longitudes": [73.2748456, 60.605514]
                },
                {
                    "id": "line11_nsk",
                    "arc": -0.85,
                    "alpha": 0.3,
                    "latitudes": [55.0364311, 55.030199],
                    "longitudes": [73.2748456, 82.920430]
                },
                {
                    "id": "line12_nsk",
                    "alpha": 0,
                    "color": "#E5343D",
                    "latitudes": [55.0364311, 55.030199],
                    "longitudes": [73.2748456, 82.920430]
                },
                {
                    "id": "line13_vld",
                    "arc": -0.85,
                    "alpha": 0.3,
                    "latitudes": [55.0364311, 43.116418],
                    "longitudes": [73.2748456, 131.882475]
                },
                {
                    "id": "line14_vld",
                    "alpha": 0,
                    "color": "#E5343D",
                    "latitudes": [55.0364311, 43.116418],
                    "longitudes": [73.2748456, 131.882475]
                },
                {
                    "id": "line15_sg",
                    "arc": -0.85,
                    "alpha": 0.3,
                    "latitudes": [55.0364311, 1.28967],
                    "longitudes": [73.2748456, 103.85007]
                },
                {
                    "id": "line16_sg",
                    "alpha": 0,
                    "color": "#E5343D",
                    "latitudes": [55.0364311, 1.28967],
                    "longitudes": [73.2748456, 103.85007]
                },
                {
                    "id": "line17",
                    "arc": -0.85,
                    "alpha": 0.3,
                    "latitudes": [55.0364311, 22.2855200],
                    "longitudes": [73.2748456, 114.1576900]
                },
                {
                    "id": "line18",
                    "alpha": 0,
                    "color": "#E5343D",
                    "latitudes": [55.0364311, 22.2855200],
                    "longitudes": [73.2748456, 114.1576900]
                },
                {
                    "id": "line19",
                    "arc": -0.85,
                    "alpha": 0.3,
                    "latitudes": [55.0364311, -6.21462],
                    "longitudes": [73.2748456, 106.84513]
                },
                {
                    "id": "line20",
                    "alpha": 0,
                    "color": "#E5343D",
                    "latitudes": [55.0364311, -6.21462],
                    "longitudes": [73.2748456, 106.84513]
                },
                {
                    "id": "line21",
                    "arc": -0.85,
                    "alpha": 0.3,
                    "latitudes": [55.0364311, 47.3666700],
                    "longitudes": [73.2748456, 8.5500000]
                },
                {
                    "id": "line22",
                    "alpha": 0,
                    "color": "#E5343D",
                    "latitudes": [55.0364311, 47.3666700],
                    "longitudes": [73.2748456, 8.5500000]
                },
                {
                    "id": "line23",
                    "arc": -0.85,
                    "alpha": 0.3,
                    "latitudes": [55.0364311, 37.77493],
                    "longitudes": [73.2748456, -122.41942]
                },
                {
                    "id": "line24",
                    "alpha": 0,
                    "color": "#E5343D",
                    "latitudes": [55.0364311, 37.77493],
                    "longitudes": [73.2748456, -122.41942]
                }
            ],
            "images": [
                {
                    "svgPath": targetSVG,
                    "title": "Омск",
                    "latitude": 55.0364311,
                    "longitude": 73.2748456
                },
                {
                    "svgPath": targetSVG,
                    "title": "Москва",
                    "latitude": 55.75222,
                    "longitude": 37.61556
                },
                {
                    "svgPath": targetSVG,
                    "title": "Санкт-Петербург",
                    "latitude": 59.9386300,
                    "longitude": 30.3141300
                },
                {
                    "svgPath": targetSVG,
                    "title": "Казань",
                    "latitude": 55.7887400,
                    "longitude": 49.1221400
                },
                {
                    "svgPath": targetSVG,
                    "title": "Калининград",
                    "latitude": 54.707390,
                    "longitude": 20.507307
                },
                {
                    "svgPath": targetSVG,
                    "title": "Екатеринбург",
                    "latitude": 56.838607,
                    "longitude": 60.605514
                },
                {
                    "svgPath": targetSVG,
                    "title": "Новосибирск",
                    "latitude": 55.030199,
                    "longitude": 82.920430
                },
                {
                    "svgPath": targetSVG,
                    "title": "Владивосток",
                    "latitude": 43.116418,
                    "longitude": 131.882475
                },
                {
                    "svgPath": targetSVG,
                    "title": "Сингапур",
                    "latitude": 1.28967,
                    "longitude": 103.85007
                },
                {
                    "svgPath": targetSVG,
                    "title": "Гонг-Конг",
                    "latitude": 22.2855200,
                    "longitude": 114.1576900
                },
                {
                    "svgPath": targetSVG,
                    "title": "Джакарта",
                    "latitude": -6.21462,
                    "longitude": 106.84513
                },
                {
                    "svgPath": targetSVG,
                    "title": "Цюрих",
                    "latitude": 47.3666700,
                    "longitude": 8.5500000
                },
                {
                    "svgPath": targetSVG,
                    "title": "Сан-Франциско",
                    "latitude": 37.77493,
                    "longitude": -122.41942
                },
                {//Flights
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#585869",
                    "animateAlongLine": true,
                    "lineId": "line1",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.8
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#000000",
                    "alpha": 0.1,
                    "animateAlongLine": true,
                    "lineId": "line2",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.3
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#585869",
                    "animateAlongLine": true,
                    "lineId": "line3",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.8
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#000000",
                    "alpha": 0.1,
                    "animateAlongLine": true,
                    "lineId": "line4",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.3
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#585869",
                    "animateAlongLine": true,
                    "lineId": "line5_kazan",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.8
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#000000",
                    "alpha": 0.1,
                    "animateAlongLine": true,
                    "lineId": "line6_kazan",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.3
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#585869",
                    "animateAlongLine": true,
                    "lineId": "line7_kaliningrad",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.8
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#000000",
                    "alpha": 0.1,
                    "animateAlongLine": true,
                    "lineId": "line8_kaliningrad",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.3
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#585869",
                    "animateAlongLine": true,
                    "lineId": "line9_ekb",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.8
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#000000",
                    "alpha": 0.1,
                    "animateAlongLine": true,
                    "lineId": "line10_ekb",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.3
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#585869",
                    "animateAlongLine": true,
                    "lineId": "line11_nsk",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.8
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#000000",
                    "alpha": 0.1,
                    "animateAlongLine": true,
                    "lineId": "line12_nsk",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.3
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#585869",
                    "animateAlongLine": true,
                    "lineId": "line13_vld",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.8
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#000000",
                    "alpha": 0.1,
                    "animateAlongLine": true,
                    "lineId": "line14_vld",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.3
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#585869",
                    "animateAlongLine": true,
                    "lineId": "line15_sg",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.8
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#000000",
                    "alpha": 0.1,
                    "animateAlongLine": true,
                    "lineId": "line16_sg",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.3
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#585869",
                    "animateAlongLine": true,
                    "lineId": "line17",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.8
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#000000",
                    "alpha": 0.1,
                    "animateAlongLine": true,
                    "lineId": "line18",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.3
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#585869",
                    "animateAlongLine": true,
                    "lineId": "line19",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.8
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#000000",
                    "alpha": 0.1,
                    "animateAlongLine": true,
                    "lineId": "line20",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.3
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#585869",
                    "animateAlongLine": true,
                    "lineId": "line21",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.8
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#000000",
                    "alpha": 0.1,
                    "animateAlongLine": true,
                    "lineId": "line22",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.3
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#585869",
                    "animateAlongLine": true,
                    "lineId": "line23",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.8
                },
                {
                    "svgPath": planeSVG,
                    "positionOnLine": 0,
                    "color": "#000000",
                    "alpha": 0.1,
                    "animateAlongLine": true,
                    "lineId": "line24",
                    "flipDirection": false,
                    "loop": true,
                    "scale": 0.03,
                    "positionScale": 1.3
                }
            ]
        },

        "areasSettings": {
            "unlistedAreasColor": "#5b69bc"
        },

        "imagesSettings": {
            "color": "#E5343D",
            "rollOverColor": "#E5343D",
            "selectedColor": "#E5343D",
            "pauseDuration": 0.2,
            "animationDuration": 4,
            "adjustAnimationSpeed": true
        },

        "linesSettings": {
            "color": "#E5343D",
            "alpha": 0.4
        },

        "export": {
            "enabled": true
        }

    });
});