<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Adminpix</title>
        <link rel="shortcut icon" href="../assets/dist/img/favicon.png" type="image/x-icon">
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
        <script>
            WebFont.load({
                google: {families: ['Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i']},
                active: function () {
                    sessionStorage.fonts = true;
                }
            });
        </script>
        <!-- START GLOBAL MANDATORY STYLE -->
        <link href="../assets/dist/css/base.css" rel="stylesheet" type="text/css">
        <!-- START THEME LAYOUT STYLE -->
        <link href="../assets/dist/css/style.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="hold-transition fixed sidebar-mini">
        
        <!-- Content Wrapper -->
        <div class="login-wrapper">
            <div class="container-center">
                <div class="panel panel-bd">
                    <div class="panel-heading">
                        <div class="view-header">
                            <div class="header-title">
                                <h3>Авторизация</h3>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <form action="auth.php" id="loginForm" method="get">
                            <div class="form-group">
                                <label class="control-label">Имя пользователя (user, hr, gos, edu)</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                                    <input id="username" type="text" class="form-control" name="role" placeholder="Имя пользователя" required="required">
                                </div>                              
                            </div>
                            <div class="form-group">
                                <label class="control-label">Пароль</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                    <input id="pass" type="password" class="form-control" name="password" placeholder="Пароль" required="required">
                                </div>                                
                            </div>
                            <div>
                                <input type="submit" class="btn btn-base btn-login" value="Вход">                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>  <!-- /.content-wrapper -->

        
        <!-- START CORE PLUGINS -->
        <script src="../assets/plugins/jQuery/jquery-1.12.4.min.js"></script>
        <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>