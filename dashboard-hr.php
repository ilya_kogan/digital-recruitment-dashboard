<?php require_once( 'inc/header.php' ); ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <div class="content">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="header-icon">
                            <i class="pe-7s-graph1"></i>
                        </div>
                        <div class="header-title">
                            <h1>Дашборд</h1>
                            <small>HR-специалиста</small>
                            <ol class="breadcrumb">
                                <li><a href="index.html"><i class="pe-7s-home"></i> Home</a></li>
                                <li><a href="#">Charts</a></li>
                                <li class="active">Flot Charts</li>
                            </ol>
                        </div>
                    </div> <!-- /. Content Header (Page header) -->

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Текущие вакансии</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">                                        
                                        <table class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Должность</th>
                                                    <th>Отдел</th>
                                                    <th>Зарплата</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>1</td>
                                                    <td>Программист</td>
                                                    <td>Разработки</td>
                                                    <td>25000</td>
                                                </tr> 
                                                <tr>
                                                    <td>2</td>
                                                    <td>Бухгалтер</td>
                                                    <td>Финансовый</td>
                                                    <td>23000</td>
                                                </tr>
                                                <tr>
                                                    <td>3</td>
                                                    <td>Менеджер</td>
                                                    <td>Реализации</td>
                                                    <td>20000</td>
                                                </tr>
                                                <tr>
                                                    <td>4</td>
                                                    <td>Программист</td>
                                                    <td>Разработки</td>
                                                    <td>15000</td>
                                                </tr>
                                                <tr>
                                                    <td>5</td>
                                                    <td>Программист</td>
                                                    <td>Разработки</td>
                                                    <td>27000</td>
                                                </tr>                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6">
                            <div class="row stat_area">
                                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-6">
                                    <div class="statistic-box dash2">
                                        <i class="ti-world statistic_icon"></i>
                                        <div class=small>Всего сотрудников</div>
                                        <h2><span class=count-number>789</span>K <span class=slight><i class="fa fa-play fa-rotate-270 text-warning"> </i> +29%</span></h2>
                                        <div class="sparkline3 text-center"></div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-6">
                                    <div class="statistic-box dash2">
                                        <i class="ti-user statistic_icon"></i>
                                        <div class=small>Цифровых профилей</div>
                                        <h2><span class=count-number>321</span>M <span class=slight><i class="fa fa-play fa-rotate-90 c-white"> </i> +10%</span> </h2>
                                        <div class="sparkline1 text-center"></div>
                                    </div>
                                </div>
                                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-6">
                                    <div class="statistic-box dash2">
                                        <i class="ti-bag statistic_icon"></i>
                                        <div class=small>Зарплата</div>
                                        <h2><span class=count-number>5489</span> руб. <span class=slight><i class="fa fa-play fa-rotate-90 c-white"> </i> +24%</span></h2>
                                        <div class="sparkline4 text-center"></div>
                                    </div>
                                </div>
                                <div class="col-xs-6 hidden-sm col-md-3 col-lg-6">
                                    <div class="statistic-box dash2">
                                        <i class="ti-server statistic_icon"></i>
                                        <div class=small>Текучка кадров</div>
                                        <h2><span class=count-number>696</span>K <span class=slight><i class="fa fa-play fa-rotate-270 text-warning"> </i> +28%</span></h2>
                                        <div class="sparkline2 text-center"></div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Список сотрудников</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">                                        
                                        <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Имя</th>
                                                    <th>Фамилия</th>
                                                    <th>Должность/Отдел</th>
                                                    <th>Возраст</th>
                                                    <th>Принят</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ( $users as $key => $user ) : ?>
                                                    <tr>
                                                        <td><?php echo $key + 1; ?></td>
                                                        <td><a href="/profile.php?role=<?php echo $_GET['role']; ?>&first_name=<?php echo $user[0]; ?>&last_name=<?php echo $user[1]; ?>"><?php echo $user[0]; ?></a></td>
                                                        <td><a href="/profile.php?role=<?php echo $_GET['role']; ?>&first_name=<?php echo $user[0]; ?>&last_name=<?php echo $user[1]; ?>"><?php echo $user[1]; ?></a></td>
                                                        <td><?php echo $user[2]; ?></td>
                                                        <td><?php echo $user[3]; ?></td>
                                                        <td><?php echo $user[4]; ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-lg-4">
                            <div class="panel panel-bd lobidisable">
                                <div class=panel-heading>
                                    <div class=panel-title>
                                        <i class=ti-email></i>
                                        <h4>Лидеры по компетенциям</h4>
                                    </div>
                                </div>

                                <div class=panel-body>
                                    <div class=message_inner>
                                        <div class=message_widgets>
                                            <?php foreach ( $users as $key => $user ) : ?>
                                                <a href="#">
                                                    <div class=inbox-item>
                                                        <div class=inbox-item-img><img src="../assets/dist/img/avatar<?php echo rand(2, 5); ?>.png" class=img-circle alt=""></div>
                                                         <strong class=inbox-item-author><?php echo $user[0] . ' ' . $user[1]; ?> (ФГО/ИСТ-131)</strong>
                                                        <span class=inbox-item-date>-13:40 PM</span>
                                                        <p class=inbox-item-text>PHP, C++, HTML</p>
                                                        <span class="profile-status available pull-right"></span>
                                                    </div>
                                                </a>
                                            <?php endforeach; ?>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 hidden-sm hidden-xs">
                            <div class="panel panel-bd lobidisable lg-mb0">
                                <div class=panel-heading>
                                    <div class=panel-title>
                                        <i class=ti-archive></i>
                                        <h4>Календарь аттестаций </h4>
                                    </div>
                                </div>
                                <div class=panel-body>
                                    <div class=monthly_calender>
                                        <div class=monthly id=m_calendar></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 hidden-xs">
                            <div class="panel panel-bd lobidisable lg-mb0">
                                <div class=panel-heading>
                                    <div class=panel-title>
                                        <i class=ti-stats-up></i>
                                        <h4>Последние активности
                                        </h4>
                                    </div>
                                </div>
                                <div class=panel-body>
                                    <ul class="activity-list list-unstyled">
                                        <li class=activity-purple>
                                            <small class=text-muted>9 минут назад</small>
                                            <p><span class="label label-success label-pill">Обновились</span> данные по компетенциям</p>
                                        </li>
                                        <li class=activity-danger>
                                            <small class=text-muted>15 минут назад</small>
                                            <p>Вам ответил Геннадий Петров</p>
                                        </li>
                                        <li class=activity-warning>
                                            <small class=text-muted>22 минут назад</small>
                                            <p><span class="label label-success label-pill">Обновились</span> данные по университетам</p>
                                        </li>
                                        <li class=activity-primary>
                                            <small class=text-muted>30 минут назад</small>
                                            <p>Вы изменили данные в профиле</p>
                                        </li>
                                        <li>
                                            <small class=text-muted>35 минут назад</small>
                                            <p>Вы обновили пароль</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>                    

                    <div class="row">
                        <!-- Polar Chart 1 -->
                        <div class="col-sm-6 col-md-4">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Информационные технологии</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="polarChart" height="310"></canvas>
                                </div>
                            </div>

                            <div class="statistic-box dash2">
                                <i class="ti-server statistic_icon"></i>
                                <div class=small>Просмотров профиля</div>
                                <h2><span class=count-number>696</span>K <span class=slight><i class="fa fa-play fa-rotate-270 text-warning"> </i> +28%</span></h2>
                                <div class="sparkline2 text-center"></div>
                            </div>
                        </div>

                        <!-- Polar Chart 2 -->
                        <div class="col-sm-6 col-md-4">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Дизайн</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="polarChart_2" height="310"></canvas>
                                </div>
                            </div>

                            <div class="statistic-box dash2">
                                <i class="ti-bag statistic_icon"></i>
                                <div class=small>Зарплата по рынку</div>
                                <h2><span class=count-number>5489</span> руб. <span class=slight><i class="fa fa-play fa-rotate-90 c-white"> </i> +24%</span></h2>
                                <div class="sparkline4 text-center"></div>
                            </div>
                        </div>

                        <!-- Polar Chart 3 -->
                        <div class="col-sm-6 col-md-4">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Управление проектами и бизнес-анализ</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="polarChart_3" height="310"></canvas>
                                </div>
                            </div>

                            <div class="statistic-box dash2">
                                <i class="ti-world statistic_icon"></i>
                                <div class=small>Количество вакансий</div>
                                <h2><span class=count-number>789</span>K <span class=slight><i class="fa fa-play fa-rotate-270 text-warning"> </i> +29%</span></h2>
                                <div class="sparkline3 text-center"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

<?php require_once( 'inc/footer.php' ); ?>