<?php require_once( 'inc/header.php' ); ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <div class="content">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="header-icon">
                            <i class="pe-7s-box1"></i>
                        </div>
                        <div class="header-title">
                            <h1>База Данных</h1>
                            <small>Компетенции</small>                            
                        </div>
                    </div> <!-- /. Content Header (Page header) -->
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="panel lobidisable panel-bd">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Информационные технологии</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Компетенция</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Основы программирования: типы и структуры данных</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Паттерны проектирования и основные подходы к архитектуре систем</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>Теория алгоритмов</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">4</th>
                                                    <td>Теория машинного обучения</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">5</th>
                                                    <td>Теория облачных технологий</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">6</th>
                                                    <td>Теория Blockchain/ распределенный реестр</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">7</th>
                                                    <td>Теория защиты информации</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">8</th>
                                                    <td>Криптография</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">9</th>
                                                    <td>Java</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">10</th>
                                                    <td>C++</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">11</th>
                                                    <td>C#</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">12</th>
                                                    <td>SQL</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">13</th>
                                                    <td>Python</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">14</th>
                                                    <td>Scala</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">15</th>
                                                    <td>R</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">16</th>
                                                    <td>Go (Golang)</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">17</th>
                                                    <td>Ruby</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">18</th>
                                                    <td>.Net</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">19</th>
                                                    <td>1С</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">20</th>
                                                    <td>PHP</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">21</th>
                                                    <td>JavaScript</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">22</th>
                                                    <td>HTML CSS</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">23</th>
                                                    <td>Swift</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">24</th>
                                                    <td>Visual Basic</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">25</th>
                                                    <td>Django</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">26</th>
                                                    <td>jQuery</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">27</th>
                                                    <td>React</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">28</th>
                                                    <td>Мобильная разработка iOs</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">29</th>
                                                    <td>Мобильная разработка Android</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">30</th>
                                                    <td>Unity 3D</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">31</th>
                                                    <td>Unreal Engine</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="panel lobidisable panel-bd">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Дизайн</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Компетенция</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Разработка UX/UI</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Проектирование и дизайн мобильных интерфейсов</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>Проектирование и дизайн веб-интерфейсов</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">4</th>
                                                    <td>Инфографика</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">5</th>
                                                    <td>Adobe Photoshop/ Adobe Illustrator</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">6</th>
                                                    <td>Sketch</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">7</th>
                                                    <td>Figma</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-sm-4">
                            <div class="panel lobidisable panel-bd">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Управление проектами и бизнес-анализ</h4>
                                    </div>
                                </div>
                                <div class="panel-body">                                    
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Компетенция</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th scope="row">1</th>
                                                    <td>Методы моделирования и аналитика бизнес-процессов</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">2</th>
                                                    <td>Product Management</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">3</th>
                                                    <td>Project Management</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">4</th>
                                                    <td>Гибкие методологии управления проектами</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">5</th>
                                                    <td>Юридическая и нормативная база в ИТ</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">6</th>
                                                    <td>Финансовое планирование</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">7</th>
                                                    <td>Управление инвестициями</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">8</th>
                                                    <td>Управления финансовыми рисками</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">9</th>
                                                    <td>Бренд-стратегия</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">10</th>
                                                    <td>Контекстная реклама</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">11</th>
                                                    <td>Мобильный маркетинг</td>
                                                </tr>
                                                <tr>
                                                    <th scope="row">12</th>
                                                    <td>Трейд-маркетинг</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                    
                </div> <!-- /.main content -->
            </div>
			
<?php require_once( 'inc/footer.php' ); ?>