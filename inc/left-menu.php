<!-- Tab panes -->
<aside class="main-sidebar">
	<!-- sidebar -->
	<div class="sidebar">
		<!-- sidebar menu -->
		<ul class="sidebar-menu">
			<li class="treeview">
				<a href="<?php echo ( isset( $_GET['role'] ) ? '/dashboard-' . $_GET['role'] . '.php?role=' . $_GET['role'] : '' ); ?>">
					<i class="ti-home"></i><span>Дашборд</span>					
				</a>				
			</li>			
			<li class="treeview">
				<a href="<?php echo ( isset( $_GET['role'] ) ? '/database.php?role=' . $_GET['role'] : '' ); ?>">
					<i class="ti-notepad"></i> <span>База Данных</span>					
				</a>				
			</li>			
		</ul>
	</div> <!-- /.sidebar -->
</aside>