<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Панель управления</title>
        <link rel="shortcut icon" href="../assets/dist/img/favicon.png" type="image/x-icon">
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
        <script>
            WebFont.load({
                google: {families: ['Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i']},
                active: function () {
                    sessionStorage.fonts = true;
                }
            });
        </script>
        <!-- START GLOBAL MANDATORY STYLE -->
        <link href="../assets/dist/css/base.css" rel="stylesheet" type="text/css">
        <!-- START PAGE LABEL PLUGINS --> 
        <link href="../assets/plugins/toastr/toastr.min.css" rel=stylesheet type="text/css"/>
        <link href="../assets/plugins/emojionearea/emojionearea.min.css" rel=stylesheet type="text/css"/>
        <link href="../assets/plugins/monthly/monthly.min.css" rel=stylesheet type="text/css"/>
        <link href="../assets/plugins/amcharts/export.css" rel=stylesheet type="text/css"/>
        <!-- START THEME LAYOUT STYLE -->
        <link href="../assets/dist/css/style.css" rel="stylesheet" type="text/css"/>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="hold-transition fixed sidebar-mini">
        <?php
            $users = array(
                array( 'Иванов', 'Иван', 'Менеджер', '25', '10.07.2020' ),
                array( 'Петр', 'Петров', 'Программист', '25', '10.07.2020' ),
                array( 'Сидоров', 'Анатолий', 'Бухгалтер', '25', '10.07.2020' ),
                array( 'Донской', 'Борис', 'Экономист', '25', '10.07.2020' ),
                array( 'Лавров', 'Дементий', 'Веб-Разработчик', '25', '10.07.2020' ),
                array( 'Грозная', 'Георгий', 'Программист', '25', '10.07.2020' ),
                array( 'Сыпкина', 'Клеопатра', 'Менеджер', '25', '10.07.2020' ),
                array( 'Ясный', 'Аристарх', 'Переводчик', '25', '10.07.2020' ),
                array( 'Бубнов', 'Валерий', 'Экономист', '25', '10.07.2020' ),
                array( 'Черная', 'Ясения', 'Веб-Разработчик', '25', '10.07.2020' ),
                array( 'Невезучих', 'Клементий', 'Менеджер', '25', '10.07.2020' ),
                array( 'Корнеева', 'Ираида', 'Веб-Разработчик', '25', '10.07.2020' ),
                array( 'Правый', 'Владимир', 'Переводчик', '25', '10.07.2020' ),
                array( 'Левая', 'Ирина', 'Менеджер', '25', '10.07.2020' ),
                array( 'Кристальный', 'Петр', 'Программист', '25', '10.07.2020' ),
                array( 'Человечный', 'Роман', 'Менеджер', '25', '10.07.2020' ),
                array( 'Ильин', 'Илья', 'Менеджер', '25', '10.07.2020' ),
                array( 'Пирогов', 'Василий', 'Менеджер', '25', '10.07.2020' ),
                array( 'Землекоп', 'Леонид', '', '25', '10.07.2020' ),
                array( 'Апостол', 'Юлия', 'Секретарь', '25', '10.07.2020' ),
            );

            $messages['user'] = array( 'Уважаемый родитель, на портале доступна ведомость с оценками Вашего ребенка за 2 контрольную неделю текущего семестра!', 'Уважаемый родитель! Зафиксировано 2 дня подряд - отсутствие вашего ребенка в учебном процессе университета!', 'Внимание! Необходимо ваше внимание к посещаемости вашим ребенком - занятий на следующей неделе!', 'Система мониторинга качества учебного процесса прогнозирует снижение успеваемости в связи с пропуском более 25% занятий', 'Уважаемый родитель! К сожалению, в текущем семестре ваш ребенок пропустил более 32% учебных занятий и с вероятностью 40% - сдача сессии будет очень очень сложной! Надеемся на вашу активную помощь вашему ребенку!' );

            $messages['hr'] = array( 'Уважаемый HRD ПАО "Сбербанк России"! В системе ПРЕДИКТИВНОЙ аналитики сформированы - срезы по итогам поиска и отбора CDO - для западно-сибирского федерального округа!', 'Уважаемый HRD ПАО "Сбербанк России"! Зафиксировано значительное изменение стоимости привлечения заявок от кандидатов на массовую вакансию, относящуюся к критической HR-инфраструктуре и репутационным рискам компании!', 'Система ПРЕДИКТИВНОЙ аналитики сформировала предложения по корректировке бюджета на HR-маркетинг в 2 регионах по итогам анализа данных о стоимости закрытия вакансий в марте 2019 года!', 'Примите или отклоните предложения системы ПРЕДИКТИВНОЙ аналитики в срок до 15 марта 2019 года, либо возрастает вероятность невыполнения планового задания!', 'По итогам дофинансирования процесса отбора и адаптации персонала в марте 2019 года в 2 регионах - приняты предложения по дофинансированию! Успешного дня!' ); 

            $messages['gos'] = array( 'В лично кабинете сформирован отчет по изменению уровня компетентностного профиля сотрудников министерства, не завершивших процедуру испытательного срока и/или адаптации!', 'Примите приглашение по совместному участию в спартакиаде Министерства труда и социального развития Омской области! Корпоративный дух - наше все!', 'Система ПРЕДИКТИВНОЙ аналитики установила взаимосвязь - между утечкой senior-программистов из Омской области в 3 квартале 2018 года - с значительным улучшением значимых условий труда в регионах - угрозах!', 'По итогам регрессионного' ); 

            $messages['edu'] = array( 'Уважаемый CDO! Система ПРЕДИКТИВНОЙ аналитики сформировала отчет с отклонениями в успеваемости наиболее важной группы магистрантов-очников!', 'Уважаемый Максим! Поздравляем Вас с Днем Рождения! Желаем Вам всего самого наилучшего! С Уважением Система ПРЕДИКТИВНОЙ аналитики!', 'Добавлен предварительный отчет по итогам выполнения планового задания за 1 полугодие 2019 года!', 'Зафиксировано повышение на 39% количества вакансий - CDO в западно-сибирском федеральном округе!' );
        ?>
        
        <!-- Preloader -->
        <div class="preloader"></div>
       
        <!-- Site wrapper -->
        <div class="wrapper">
            <header class="main-header"> 
                <a href="<?php echo ( isset( $_GET['role'] ) ? '/dashboard-' . $_GET['role'] . '.php' : '' ); ?>" class="logo"> <!-- Logo -->
                    <span class="logo-mini">
                        <b>DR</b> <small>- Admin</small>
                        <span class="badge">β</span>
                    </span>
                    <span class="logo-lg">
                        <b>DR</b> <small>- Admin</small>
                        <span class="badge">β</span>
                    </span>
                </a>
                <!-- Header Navbar -->
                <nav class="navbar navbar-static-top">
                    <a href="#" class="sidebar-toggle hidden-sm hidden-md hidden-lg" data-toggle="offcanvas" role="button"> <!-- Sidebar toggle button-->
                        <span class="sr-only">Toggle navigation</span>
                        <span class="ti-menu-alt"></span>
                    </a>
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <li>
                                <form class="navbar-form hidden-xs" role="search">
                                    <div class="input-group add-on">
                                        <input class="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit" data-toggle="tooltip" data-placement="bottom" title="Search"><i class="ti-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </li>
                            <li class="dropdown dropdown-settings">
                                <a href="#" class="dropdown-toggle bubbly-button" data-toggle="dropdown"> <i class="fa fa-bell-o"></i><span class="badge fadeAnim">2</span></a>
                                <div class="notification-box dropdown-menu animated bounceIn">
                                    <div class="notification-header">
                                        <h4>2 новых оповещения</h4>
                                        <a href="#">ощистить</a>
                                    </div>
                                    <div class="notification-body">
                                        <ul class="notification_inner">
                                            <?php if ( isset( $_GET['role'] ) && ! empty( isset( $_GET['role'] ) ) ) : ?>
                                                <?php foreach ( $messages[ $_GET['role'] ] as $message ) : ?>
                                                    <li>
                                                      <a href="#" class="single-notification">
                                                         <div class="notification-img">
                                                            <i class="fa fa-commenting"></i>
                                                         </div>
                                                         <div class="notification-txt">
                                                            <h4><?php echo $message; ?></h4>
                                                            <span><?php echo rand( 5, 60 ); ?> минут назад</span>
                                                         </div>
                                                      </a>
                                                    </li>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                    <div class="notification-footer">
                                        <a href="#">Смотреть все оповещения<i class="fa fa-long-arrow-right"></i></a>
                                    </div>
                                </div>
                            </li>
                            <li class="dropdown dropdown-settings">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="ti-email"></i><span class="badge fadeAnim">3</span></a>
                                <div class="dropdown-menu msg_box">
                                    <div class="message-header">
                                        <h4>3 new messages</h4>
                                    </div>
                                    <div class="message-body">
                                        <div class=message_inner2>
                                            <div class=message_widgets>
                                                <a href="#">
                                                    <div class=inbox-item>
                                                        <div class=inbox-item-img><img src="../assets/dist/img/avatar.png" class=img-circle alt=""></div>
                                                        <strong class=inbox-item-author>Farzana Yasmin</strong>
                                                        <span class=inbox-item-date>-13:40 PM</span>
                                                        <p class=inbox-item-text>Hey! there I'm available...</p>
                                                        <span class="profile-status available pull-right"></span>
                                                    </div>
                                                </a>
                                                <a href="#">
                                                    <div class=inbox-item>
                                                        <div class=inbox-item-img><img src="../assets/dist/img/avatar2.png" class=img-circle alt=""></div>
                                                        <strong class=inbox-item-author>Mubin Khan</strong>
                                                        <span class=inbox-item-date>-13:40 PM</span>
                                                        <p class=inbox-item-text>Hey! there I'm available...</p>
                                                        <span class="profile-status away pull-right"></span>
                                                    </div>
                                                </a>
                                                <a href="#">
                                                    <div class=inbox-item>
                                                        <div class=inbox-item-img><img src="../assets/dist/img/avatar3.png" class=img-circle alt=""></div>
                                                        <strong class=inbox-item-author>Mozammel Hoque</strong>
                                                        <span class=inbox-item-date>-13:40 PM</span>
                                                        <p class=inbox-item-text>Hey! there I'm available...</p>
                                                        <span class="profile-status busy pull-right"></span>
                                                    </div>
                                                </a>
                                                <a href="#">
                                                    <div class=inbox-item>
                                                        <div class=inbox-item-img><img src="../assets/dist/img/avatar4.png" class=img-circle alt=""></div>
                                                        <strong class=inbox-item-author>Tanzil Ahmed</strong>
                                                        <span class=inbox-item-date>-13:40 PM</span>
                                                        <p class=inbox-item-text>Hey! there I'm  available...</p>
                                                        <span class="profile-status offline pull-right"></span>
                                                    </div>
                                                </a>
                                                <a href="#">
                                                    <div class=inbox-item>
                                                        <div class=inbox-item-img><img src="../assets/dist/img/avatar5.png" class=img-circle alt=""></div>
                                                        <strong class=inbox-item-author>Amir Khan</strong>
                                                        <span class=inbox-item-date>-13:40 PM</span>
                                                        <p class=inbox-item-text>Hey! there I'm available...</p>
                                                        <span class="profile-status available pull-right"></span>
                                                    </div>
                                                </a>
                                                <a href="#">
                                                    <div class=inbox-item>
                                                        <div class=inbox-item-img><img src="../assets/dist/img/avatar.png" class=img-circle alt=""></div>
                                                        <strong class=inbox-item-author>Salman Khan</strong>
                                                        <span class=inbox-item-date>-13:40 PM</span>
                                                        <p class=inbox-item-text>Hey! there I'm available...</p>
                                                        <span class="profile-status available pull-right"></span>
                                                    </div>
                                                </a>
                                                <a href="#">
                                                    <div class=inbox-item>
                                                        <div class=inbox-item-img><img src="../assets/dist/img/avatar.png" class=img-circle alt=""></div>
                                                        <strong class=inbox-item-author>Farzana Yasmin</strong>
                                                        <span class=inbox-item-date>-13:40 PM</span>
                                                        <p class=inbox-item-text>Hey! there I'm available...</p>
                                                        <span class="profile-status available pull-right"></span>
                                                    </div>
                                                </a>
                                                <a href="#">
                                                    <div class=inbox-item>
                                                        <div class=inbox-item-img><img src="../assets/dist/img/avatar4.png" class=img-circle alt=""></div>
                                                        <strong class=inbox-item-author>Tanzil Ahmed</strong>
                                                        <span class=inbox-item-date>-13:40 PM</span>
                                                        <p class=inbox-item-text>Hey! there I'm available...</p>
                                                        <span class="profile-status offline pull-right"></span>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="message-footer">
                                        <a href="#">see all messages<i class="fa fa-long-arrow-right"></i></a>
                                     </div>
                                </div>
                            </li>
                            <li class="dropdown dropdown-settings">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="ti-menu"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">Superadmin</a></li>
                                    <li><a href="#">Admin</a></li>
                                    <li><a href="#">HR</a></li>
                                    <li><a href="#">Manager</a></li>
                                    <li><a href="#">Editor</a></li>
                                    <li><a href="#">Subscriber</a></li>
                                    <li><a href="#">User</a></li>
                                </ul>
                            </li>
                            <li class="dropdown dropdown-user">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="ti-user"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a href="profile.html"><i class="ti-user"></i> User Profile</a></li>
                                    <li><a href="#"><i class="ti-settings"></i> Settings</a></li>
                                    <li><a href="login.html"><i class="ti-key"></i> Logout</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>

                <?php require_once( 'inc/left-menu.php' ); ?>                
            </header>