$(document).ready(function () {

    "use strict"; // Start of use strict

    //bar chart 1
    var ctxIT_1 = document.getElementById("barChartIT");
    var myChart = new Chart(ctxIT_1, {
        type: 'bar',
        data: {
            labels: ["Основы программирования: типы и структуры данных", "Паттерны проектирования и основные подходы к архитектуре систем", "Теория алгоритмов", "Теория машинного обучения", "Теория облачных технологий", "Теория Blockchain/ распределенный реестр", "Теория защиты информации", "Криптография", "Java", "C++", "C#", "SQL", "Python", "Scala", "R", "Go (Golang)", "Ruby", ".Net", "1С", "PHP", "JavaScript", "HTML CSS", "Swift", "Visual Basic", "Django", "jQuery", "React", "Мобильная разработка iOs", "Мобильная разработка Android", "Unity 3D", "Unreal Engine"],
            datasets: [
                {
                    label: "Виктор Александров - До",
                    data: [56, 33, 32, 64, 27, 19, 86, 84, 32, 46, 25, 22, 25, 67, 98, 46, 87, 78, 76, 69, 74, 35, 73, 67, 46, 54, 84, 74, 36, 68, 98],
                    borderColor: "rgba(87, 120, 123 , 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(87, 120, 123 , 0.5)" 
                },
                {
                    label: "Виктор Александров - После",
                    data: [88, 77, 43, 88, 56, 44, 93, 87, 45, 78, 34, 54, 56, 76, 99, 87, 88, 90, 87, 54, 88, 45, 78, 78, 67, 87, 88, 74, 46, 78, 98],
                    borderColor: "rgba(0,0,0,0.09)",
                    borderWidth: "0",
                    backgroundColor: "rgba(0,0,0,0.07)"
                }
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        display: false,
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    //bar chart 2
    var ctxDesign_1 = document.getElementById("barChartDesign");
    var myChart = new Chart(ctxDesign_1, {
        type: 'bar',
        data: {
            labels: ["Разработка UX/UI", "Проектирование и дизайн мобильных интерфейсов", "Проектирование и дизайн веб-интерфейсов", "Инфографика", "Adobe Photoshop/ Adobe Illustrator", "Sketch", "Figma"],
            datasets: [
                {
                    label: "Виктор Александров - До",
                    data: [25, 67, 25, 67, 98, 46, 87],
                    borderColor: "rgba(87, 120, 123, 0.6)",
                    borderWidth: "0",
                    backgroundColor: "rgba(87, 120, 123, 0.4)"
                },
                {
                    label: "Виктор Александров - После",
                    data: [55, 89, 47, 89, 99, 67, 98],
                    borderColor: "rgba(87, 120, 123, 0.7",
                    borderWidth: "0",
                    backgroundColor: "rgba(87, 120, 123, 0.5)"
                }
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        display: false,
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    //bar chart 3
    var ctxPM_1 = document.getElementById("barChartPM");
    var myChart = new Chart(ctxPM_1, {
        type: 'bar',
        data: {
            labels: ["Методы моделирования и аналитика бизнес-процессов", "Product Management", "Project Management", "Гибкие методологии управления проектами", "Юридическая и нормативная база в ИТ", "Финансовое планирование", "Управление инвестициями", "Управления финансовыми рисками", "Бренд-стратегия", "Контекстная реклама", "Мобильный маркетинг", "Трейд-маркетинг"],
            datasets: [
                {
                    label: "Виктор Александров - До",
                    data: [67, 98, 46, 87, 25, 67, 25, 78, 54, 43, 77],
                    borderColor: "rgba(87, 120, 123, 0.6)",
                    borderWidth: "0",
                    backgroundColor: "rgba(87, 120, 123, 0.4)"
                },
                {
                    label: "Виктор Александров - После",
                    data: [78, 88, 87, 97, 56, 88, 54, 88, 78, 78, 89],
                    borderColor: "rgba(87, 120, 123, 0.7",
                    borderWidth: "0",
                    backgroundColor: "rgba(87, 120, 123, 0.5)"
                }
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        display: false,
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    //radar chart 1
    var ctxIT_2 = document.getElementById("radarChartIT");
    var myChart = new Chart(ctxIT_2, {
        type: 'radar',
        data: {
            labels: ["Основы программирования: типы и структуры данных", "Паттерны проектирования и основные подходы к архитектуре систем", "Теория алгоритмов", "Теория машинного обучения", "Теория облачных технологий", "Теория Blockchain/ распределенный реестр", "Теория защиты информации", "Криптография", "Java", "C++", "C#", "SQL", "Python", "Scala", "R", "Go (Golang)", "Ruby", ".Net", "1С", "PHP", "JavaScript", "HTML CSS", "Swift", "Visual Basic", "Django", "jQuery", "React", "Мобильная разработка iOs", "Мобильная разработка Android", "Unity 3D", "Unreal Engine"],
            datasets: [
                {
                    label: "Виктор Александров - До",
                    data: [56, 33, 32, 64, 27, 19, 86, 84, 32, 46, 25, 22, 25, 67, 98, 46, 87, 78, 76, 69, 74, 35, 73, 67, 46, 54, 84, 74, 36, 68, 98],
                    borderColor: "rgba(87, 120, 123, 0.6)",
                    borderWidth: "1",
                    backgroundColor: "rgba(87, 120, 123, 0.4)"
                },
                {
                    label: "Виктор Александров - После",
                    data: [88, 77, 43, 88, 56, 44, 93, 87, 45, 78, 34, 54, 56, 76, 99, 87, 88, 90, 87, 54, 88, 45, 78, 78, 67, 87, 88, 74, 46, 78, 98],
                    borderColor: "rgba(87, 120, 123, 0.7",
                    borderWidth: "1",
                    backgroundColor: "rgba(87, 120, 123, 0.5)"
                }
            ]            
        },        
        options: {
            title: {
                display: true,
                text: 'Информационные технологии'
            },
            legend: {
                position: 'top',
                display: true,
                labels: {
                    fontColor: '#000'
                }
            },
            scale: {
                ticks: {
                    beginAtZero: true
                },
                pointLabels: {
                    callback: function(pointLabel, index, labels) {
                        return '';
                    }
                }
            }
        }
    });

    //radar chart 2
    var ctxDesign_2 = document.getElementById("radarChartDesign");
    var myChart = new Chart(ctxDesign_2, {
        type: 'radar',
        data: {
            labels: ["Разработка UX/UI", "Проектирование и дизайн мобильных интерфейсов", "Проектирование и дизайн веб-интерфейсов", "Инфографика", "Adobe Photoshop/ Adobe Illustrator", "Sketch", "Figma"],
            datasets: [
                {
                    label: "Виктор Александров - До",
                    data: [25, 67, 25, 67, 98, 46, 87],
                    borderColor: "rgba(87, 120, 123, 0.6)",
                    borderWidth: "1",
                    backgroundColor: "rgba(87, 120, 123, 0.4)"
                },
                {
                    label: "Виктор Александров - После",
                    data: [55, 89, 47, 89, 99, 67, 98],
                    borderColor: "rgba(87, 120, 123, 0.7",
                    borderWidth: "1",
                    backgroundColor: "rgba(87, 120, 123, 0.5)"
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'Дизайн'
            },
            legend: {
                position: 'top',
                display: true,
                labels: {
                    fontColor: '#000'
                }
            },
            scale: {
                ticks: {
                    beginAtZero: true
                },
                pointLabels: {
                    callback: function(pointLabel, index, labels) {
                        return '';
                    } 
                }
            }
        }
    });

    // //radar chart 3
    var ctxPM_2 = document.getElementById("radarChartPM");
    var myChart = new Chart(ctxPM_2, {
        type: 'radar',
        data: {
            labels: ["Методы моделирования и аналитика бизнес-процессов", "Product Management", "Project Management", "Гибкие методологии управления проектами", "Юридическая и нормативная база в ИТ", "Финансовое планирование", "Управление инвестициями", "Управления финансовыми рисками", "Бренд-стратегия", "Контекстная реклама", "Мобильный маркетинг", "Трейд-маркетинг"],
            datasets: [
                {
                    label: "Виктор Александров - До",
                    data: [67, 98, 46, 87, 25, 67, 25, 78, 54, 43, 77],
                    borderColor: "rgba(87, 120, 123, 0.6)",
                    borderWidth: "1",
                    backgroundColor: "rgba(87, 120, 123, 0.4)"
                },
                {
                    label: "Виктор Александров - После",
                    data: [78, 88, 87, 97, 56, 88, 54, 88, 78, 78, 89],
                    borderColor: "rgba(87, 120, 123, 0.7",
                    borderWidth: "1",
                    backgroundColor: "rgba(87, 120, 123, 0.5)"
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'Управление проектами и бизнес-анализ'
            },
            legend: {
                position: 'top',
                display: true,
                labels: {
                    fontColor: '#000'
                }
            },
            scale: {
                ticks: {
                    beginAtZero: true
                },
                pointLabels: {
                    callback: function(pointLabel, index, labels) {
                        return '';
                    } 
                }
            }
        }
    });

    //line chart    
    var ctx = document.getElementById("lineChart");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
                {
                    label: "My First dataset 1",
                    borderColor: "rgba(0,0,0,0.9)",
                    borderWidth: "1",
                    backgroundColor: "rgba(0,0,0,0.7)",
                    data: [22, 60, 67, 43, 12, 45, 44],
                    fill: false
                },
                {
                    label: "My Second dataset 2",
                    borderColor: "rgba(87, 120, 123, 0.8)",
                    borderWidth: "1",
                    backgroundColor: "rgba(87, 120, 123, 0.5)",
                    pointHighlightStroke: "rgba(26,179,148,1)",
                    data: [44, 33, 50, 26, 42, 33, 12],
                    fill: false
                }
            ]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Описываем соотношение спроса и предложения: вакансии cdo / предложение на рынке cdo'
            },
            tooltips: {
                mode: 'index',
                intersect: false
            },
            hover: {
                mode: 'nearest',
                intersect: true
            }

        }
    });

    //line chart 2
    var ctx = document.getElementById("lineChart_2");
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
            datasets: [{
                label: 'Unfilled',
                fill: false,
                backgroundColor: '#36a2eb',
                borderColor: '#36a2eb',
                data: [22, 60, 67, 43, 12, 45, 44],
            }, {
                label: 'Dashed',
                fill: false,
                backgroundColor: '#50c1c1',
                borderColor:'#50c1c1',
                borderDash: [5, 5],
                data: [44, 33, 50, 26, 42, 33, 12],
            }, {
                label: 'Filled',
                backgroundColor: '#ff6384',
                borderColor: '#ff6384',
                data: [55, 54, 43, 32, 67, 78, 65],
                fill: true,
            }]
        },
        options: {
            responsive: true,
            title: {
                display: true,
                text: 'Chart.js Line Chart'
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Month'
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'Value'
                    }
                }]
            }
        }
    });    

    //pie chart
    var ctx = document.getElementById("pieChart");
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            datasets: [{
                data: [45, 25, 20, 10],
                backgroundColor: [
                    "rgba(87, 120, 123, 0.9)",
                    "rgba(87, 120, 123, 0.7)",
                    "rgba(87, 120, 123, 0.5)",
                    "rgba(87, 120, 123, 0.3)"
                ],
                hoverBackgroundColor: [
                    "rgba(87, 120, 123, 0.9)",
                    "rgba(87, 120, 123, 0.7)",
                    "rgba(87, 120, 123, 0.5)",
                    "rgba(87, 120, 123, 0.3)"
                ]
            }],
            labels: [
                "green",
                "green",
                "green"
            ]
        },
        options: {
            responsive: true
        }
    });

    //doughut chart
    var ctx = document.getElementById("doughutChart");
    var myChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [45, 25, 20, 10],
                backgroundColor: [
                    "rgba(87, 120, 123, 0.9)",
                    "rgba(87, 120, 123, 0.7)",
                    "rgba(87, 120, 123, 0.5)",
                    "rgba(0,0,0,0.07)"
                ],
                hoverBackgroundColor: [
                    "rgba(87, 120, 123, 0.9)",
                    "rgba(87, 120, 123, 0.7)",
                    "rgba(87, 120, 123, 0.5)",
                    "rgba(0,0,0,0.07)"
                ]

            }],
            labels: [
                "green",
                "green",
                "green",
                "green"
            ]
        },
        options:{
            responsive: true
        }
    });

    //polar chart 1
    var ctx = document.getElementById("polarChart");
    var myChart = new Chart(ctx, {
        type: 'polarArea',
        data: {
            datasets: [{
                data: [56, 33, 32, 64, 27, 19, 86, 84, 32, 46, 25, 22, 25, 67, 98, 46, 87, 78, 76, 69, 74, 35, 73, 67, 46, 54, 84, 74, 36, 68, 98],
                backgroundColor: [
                    "rgba(87, 120, 123, 0.9)",
                    "rgba(87, 120, 123, 0.8)",
                    "rgba(87, 120, 123, 0.7)",
                    "rgba(0,0,0,0.2)",
                    "rgba(87, 120, 123, 0.5)"
                ]
            }],
            labels: [
                "green",
                "green",
                "green",
                "green"
            ]
        },
        options: {
            responsive: true
        }
    });

    //polar chart 2
    var ctx = document.getElementById("polarChart_2");
    var myChart = new Chart(ctx, {
        type: 'polarArea',
        data: {
            datasets: [{
                data: [25, 67, 25, 67, 98, 46, 87],
                backgroundColor: [
                    "rgba(87, 120, 123, 0.9)",
                    "rgba(87, 120, 123, 0.8)",
                    "rgba(87, 120, 123, 0.7)",
                    "rgba(0,0,0,0.2)",
                    "rgba(87, 120, 123, 0.5)"
                ]
            }],
            labels: [
                "green",
                "green",
                "green",
                "green"
            ]
        },
        options: {
            responsive: true
        }
    });

    //polar chart 3
    var ctx = document.getElementById("polarChart_3");
    var myChart = new Chart(ctx, {
        type: 'polarArea',
        data: {
            datasets: [{
                data: [67, 98, 46, 87, 25, 67, 25, 78, 54, 43, 77],
                backgroundColor: [
                    "rgba(87, 120, 123, 0.9)",
                    "rgba(87, 120, 123, 0.8)",
                    "rgba(87, 120, 123, 0.7)",
                    "rgba(0,0,0,0.2)",
                    "rgba(87, 120, 123, 0.5)"
                ]
            }],
            labels: [
                "green",
                "green",
                "green",
                "green"
            ]
        },
        options: {
            responsive: true
        }
    });

    // single bar chart
    var ctx = document.getElementById("singelBarChart");
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ["Sun", "Mon", "Tu", "Wed", "Th", "Fri", "Sat"],
            datasets: [
                {
                    label: "My First dataset",
                    data: [40, 55, 75, 81, 56, 55, 40],
                    borderColor: "rgba(87, 120, 123, 0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(87, 120, 123, 0.5)"
                }
            ]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });

});