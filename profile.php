<?php require_once( 'inc/header.php' ); ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <div class="content">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="header-icon"><i class="pe-7s-user-female"></i></div>
                        <div class="header-title">
                            <h1>Профиль</h1>
                            <small>Пользователя</small>
                        </div>
                    </div> <!-- /. Content Header (Page header) -->
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-header-menu">
                                        <i class="fa fa-bars"></i>
                                    </div>
                                    <div class="review-block-img">
                                        <img src="../assets/dist/img/avatar2.png" class="img-rounded" alt="">
                                    </div>
                                </div>
                                <div class="card-content">
                                    <div class="card-content-member">
                                        <h4 class="m-t-0"><?php echo $_GET['first_name'] . ' ' . $_GET['last_name']; ?></h4>
                                        <p class="m-0"><i class="pe-7s-map-marker"></i>Омск</p>
                                    </div>
                                    <div class="card-content-languages">
                                        <div class="card-content-languages-group">
                                            <div>
                                                <h4>Образование:</h4>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li>ОмГТУ</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-content-languages-group">
                                            <div>
                                                <h4>Языки:</h4>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li>Русский
                                                        <div class="fluency fluency-4"></div>
                                                    </li>
                                                    <li>English
                                                        <div class="fluency fluency-4"></div>
                                                    </li>
                                                    
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="card-content-languages-group">
                                            <div>
                                                <h4>Навыки:</h4>
                                            </div>
                                            <div>
                                                <ul>
                                                    <li>HTML,</li>
                                                    <li>PHP,</li>
                                                    <li>Ruby</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-content-summary">
                                        <p>Я IT специалист. Ищу работу, возможнен переезд.</p>
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="card-footer-stats">
                                        <div>
                                            <p>Мест работы:</p><i class="fa fa-briefcase"></i> <span>2</span>
                                        </div>
                                        <div>
                                            <p>Посещяемость страницы:</p><i class="fa fa-eye"></i> <span>350</span>
                                        </div>
                                        <div>
                                            <p>Активность:</p><span class="stats-small"> 3 дня назад</span>
                                        </div>
                                    </div>
                                    <div class="card-footer-message">
                                        <h4><i class="fa fa-comments"></i> Написать</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-8">
                            <div class="row">
                                <div class="col-sm-12 col-md-6">
                                    <div class="rating-block">
                                        <h4>Средний рейтинг</h4>
                                        <h2 class="m-b-20">4.3 <small>/ 5</small></h2>
                                        <button type="button" class="btn btn-base btn-sm" aria-label="Left Align">
                                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                        </button>
                                        <button type="button" class="btn btn-base btn-sm" aria-label="Left Align">
                                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                        </button>
                                        <button type="button" class="btn btn-base btn-sm" aria-label="Left Align">
                                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                        </button>
                                        <button type="button" class="btn btn-default btn-sm" aria-label="Left Align">
                                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                        </button>
                                        <button type="button" class="btn btn-default btn-sm" aria-label="Left Align">
                                            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <h4 class="m-t-0">Рейтинг отзывов</h4>
                                    <div class="pull-left">
                                        <div class="review-number"><div>5 <span class="glyphicon glyphicon-star"></span></div></div>
                                        <div class="review-progress">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-danger progress-bar-striped p1 active" role="progressbar" aria-valuenow="5" aria-valuemin="0" aria-valuemax="5">
                                                    <span class="sr-only">90% Complete (danger)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="progress-number">1</div>
                                    </div>
                                    <div class="pull-left">
                                        <div class="review-number">
                                            <div>4 <span class="glyphicon glyphicon-star"></span></div>
                                        </div>
                                        <div class="review-progress">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-primary progress-bar-striped p2 active" role="progressbar" aria-valuenow="4" aria-valuemin="0" aria-valuemax="5">
                                                    <span class="sr-only">80% Complete (danger)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="progress-number">1</div>
                                    </div>
                                    <div class="pull-left">
                                        <div class="review-number">
                                            <div>3 <span class="glyphicon glyphicon-star"></span></div>
                                        </div>
                                        <div class="review-progress">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info progress-bar-striped p3 active" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="5">
                                                    <span class="sr-only">70% Complete (danger)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="progress-number">0</div>
                                    </div>
                                    <div class="pull-left">
                                        <div class="review-number">
                                            <div>2 <span class="glyphicon glyphicon-star"></span></div>
                                        </div>
                                        <div class="review-progress">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-warning progress-bar-striped p4 active" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="5">
                                                    <span class="sr-only">60% Complete (danger)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="progress-number">0</div>
                                    </div>
                                    <div class="pull-left">
                                        <div class="review-number">
                                            <div>1 <span class="glyphicon glyphicon-star"></span></div>
                                        </div>
                                        <div class="review-progress">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-violet progress-bar-striped p5 active" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="5">
                                                    <span class="sr-only">50% Complete (danger)</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="progress-number">0</div>
                                    </div>
                                </div>			
                            </div>	
                            <div class="review-block">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="review-block-img">
                                            <img src="../assets/dist/img/avatar.png" class="img-rounded" alt="">
                                        </div>
                                        <div class="review-block-name"><a href="#">Алексей</a></div>
                                        <div class="review-block-date">10.04.2019<br/>25 дней назад</div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="review-block-rate">
                                            <button type="button" class="btn btn-base btn-xs" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            </button>
                                            <button type="button" class="btn btn-base btn-xs" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            </button>
                                            <button type="button" class="btn btn-base btn-xs" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            </button>
                                            <button type="button" class="btn btn-default btn-xs" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            </button>
                                            <button type="button" class="btn btn-default btn-xs" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            </button>
                                        </div>
                                        <div class="review-block-title">Хороший работник</div>
                                        <div class="review-block-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="review-block-img">
                                            <img src="../assets/dist/img/avatar2.png" class="img-rounded" alt="">
                                        </div>
                                        <div class="review-block-name"><a href="#">Ирина</a></div>
                                        <div class="review-block-date">09.03.2019<br/>35 дней назад</div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="review-block-rate">
                                            <button type="button" class="btn btn-base btn-xs" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            </button>
                                            <button type="button" class="btn btn-base btn-xs" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            </button>
                                            <button type="button" class="btn btn-base btn-xs" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            </button>
                                            <button type="button" class="btn btn-default btn-xs" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            </button>
                                            <button type="button" class="btn btn-default btn-xs" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            </button>
                                        </div>
                                        <div class="review-block-title">Мало опыта</div>
                                        <div class="review-block-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </div>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="review-block-img">
                                            <img src="../assets/dist/img/avatar3.png" class="img-rounded" alt="">
                                        </div>
                                        <div class="review-block-name"><a href="#">Виктор</a></div>
                                        <div class="review-block-date">01.02.2019<br/>65 дней назад</div>
                                    </div>
                                    <div class="col-sm-9">
                                        <div class="review-block-rate">
                                            <button type="button" class="btn btn-base btn-xs" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            </button>
                                            <button type="button" class="btn btn-base btn-xs" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            </button>
                                            <button type="button" class="btn btn-base btn-xs" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            </button>
                                            <button type="button" class="btn btn-base btn-xs" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            </button>
                                            <button type="button" class="btn btn-default btn-xs" aria-label="Left Align">
                                                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                            </button>
                                        </div>
                                        <div class="review-block-title">Мало опыта</div>
                                        <div class="review-block-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type. </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>

<?php require_once( 'inc/footer.php' ); ?>