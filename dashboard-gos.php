<?php require_once( 'inc/header.php' ); ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <div class="content">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="header-icon">
                            <i class="pe-7s-graph3"></i>
                        </div>
                        <div class="header-title">
                            <h1>Дашборд</h1>
                            <small>Государства</small>                            
                        </div>
                    </div> <!-- /. Content Header (Page header) -->

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Карта покидания Омска</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div id="amchartMap3"></div>
                                </div>
                            </div>
                        </div>

                        <!-- Polar Chart 1 -->
                        <div class="col-sm-6 col-md-4">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Информационные технологии</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="polarChart" height="310"></canvas>
                                </div>
                            </div>
                        </div>

                        <!-- Polar Chart 2 -->
                        <div class="col-sm-6 col-md-4">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Дизайн</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="polarChart_2" height="310"></canvas>
                                </div>
                            </div>
                        </div>

                        <!-- Polar Chart 3 -->
                        <div class="col-sm-6 col-md-4">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Управление проектами и бизнес-анализ</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="polarChart_3" height="310"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <!-- Line Chart 1 -->
                        <div class="col-sm-12 col-md-6">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Cоотношение спроса и предложения</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="lineChart" height="140"></canvas>
                                </div>
                            </div>
                        </div>

                        <!-- Line Chart 2 -->
                        <div class="col-sm-12 col-md-6">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Прогноз предложения</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="lineChart_2" height="140"></canvas>
                                </div>
                            </div>
                        </div>                        
                    </div>                    
                </div>
            </div>

<?php require_once( 'inc/footer.php' ); ?>