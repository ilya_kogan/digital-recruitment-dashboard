<?php require_once( 'inc/header.php' ); ?>
   
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <div class="content">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="header-icon">
                            <i class="pe-7s-graph3"></i>
                        </div>
                        <div class="header-title">
                            <h1>Дашборд</h1>
                            <small>Пользователя</small>
                        </div>
                    </div> <!-- /. Content Header (Page header) -->

                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <div class="row stat_area">                            
                                <div class="col-lg-12 col-md-12">
                                    <div class="statistic-box dash2">
                                        <i class="ti-user statistic_icon"></i>
                                        <div class=small>Приглашений</div>
                                        <h2><span class=count-number>14</span> <span class=slight><i class="fa fa-play fa-rotate-270 c-white"> </i> +10%</span> </h2>
                                        <div class="sparkline1 text-center"></div>
                                        <br>
                                        <br>                                    
                                        <div class="alert alert-success alert-dismissible" role="alert" style="margin-bottom: 19px;">
                                            <p><i class="fa fa-check"></i> <a href="#">Откликнуться на приглашения</a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12 col-md-4">
                            <div class="card">
                                <div class="card-header">                                    
                                    <div class="card-header-headshot"></div>
                                </div>
                                <div class="card-content">
                                    <div class="card-content-member">
                                        <h4 class="m-t-0">Иванова Виктория Ивановна</h4>
                                        <p class="m-0"><i class="pe-7s-map-marker"></i>Омск</p>
                                    </div>                                    
                                </div>
                                <div class="card-footer">
                                    <div class="card-footer-stats">
                                        <div style="padding-bottom: 7px;"><br></div>
                                    </div>
                                </div>
                            </div>                            
                        </div>

                        <div class="col-sm-12 col-md-4" data-lobipanel-child-inner-id="mYYfuAc02K">
                            <!-- Primary Panel -->
                            <div class="panel panel-primary" data-inner-id="mYYfuAc02K" data-index="0">
                                <div class="panel-heading">
                                    <div class="panel-title" style="max-width: calc(100% - 90px);">
                                        <h4>Данные о пользователе:</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <p>
                                        <h3><b>ФИО:</b> Иванова Виктория Ивановна</h3>
                                    </p>
                                    <p>
                                        <h3><b>Образование:</b> ОмГТУ, Омск, 2010 г.в.</h3>
                                    </p>
                                    <p>
                                        <h3><b>Место работы:</b> ООО "Вектор Сервис"</h3>
                                    </p>
                                    <div class="alert alert-sm alert-info alert-dismissible" role="alert" style="margin-bottom: 0;">
                                        <p><i class="fa fa-info-circle"></i> <a href="#">Редактировать профиль</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="row stat_area">
                                <div class="col-sm-4">
                                    <div class="statistic-box dash2">
                                        <i class="ti-server statistic_icon"></i>
                                        <div class=small>Просмотров профиля</div>
                                        <h2><span class=count-number>696</span>K <span class=slight><i class="fa fa-play fa-rotate-270 text-warning"> </i> +28%</span></h2>
                                        <div class="sparkline2 text-center"></div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="statistic-box dash2">
                                        <i class="ti-bag statistic_icon"></i>
                                        <div class=small>Зарплата по рынку</div>
                                        <h2><span class=count-number>54890</span> руб. <span class=slight><i class="fa fa-play fa-rotate-90 c-white"> </i> +24%</span></h2>
                                        <div class="sparkline4 text-center"></div>
                                    </div>
                                </div> 
                                <div class="col-sm-4">
                                    <div class="statistic-box dash2">
                                        <i class="ti-world statistic_icon"></i>
                                        <div class=small>Количество вакансий</div>
                                        <h2><span class=count-number>789</span>K <span class=slight><i class="fa fa-play fa-rotate-270 text-warning"> </i> +29%</span></h2>
                                        <div class="sparkline3 text-center"></div>
                                    </div>
                                </div>                          
                            </div>
                        </div>
                    </div>                    

                    <div class="row">
                        <center><b><h2>Цифровой профиль компетенций</h2></b></center>
                        <!-- Графики --->
                        <!-- Doughnut Chart -->
                        <div class="col-sm-6 col-md-4">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Информационные технологии</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="radarChartIT" height="310"></canvas>
                                </div>
                            </div>
                        </div>
                        <!-- Polar Chart -->
                        <div class="col-sm-6 col-md-4">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Дизайн</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="radarChartDesign" height="310"></canvas>
                                </div>
                            </div>
                        </div>
                        <!-- Single Bar Chart -->
                        <div class="col-sm-6 col-md-4">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Управление проектами и бизнес-анализ</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="radarChartPM" height="310"></canvas>
                                </div>
                            </div>
                        </div>

                        <!-- Bar Chart -->
                        <div class="col-sm-6 col-md-4">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Информационные технологии</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="barChartIT" height="200"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Дизайн</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="barChartDesign" height="200"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-4">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Управление проектами и бизнес-анализ</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="barChartPM" height="200"></canvas>
                                </div>
                            </div>
                        </div>                                                                                               
                    </div>

                    <div class="row">
                        <div class="col-md-4 hidden-sm hidden-xs">
                            <div class="panel panel-bd lobidisable lg-mb0">
                                <div class=panel-heading>
                                    <div class=panel-title>
                                        <i class=ti-archive></i>
                                        <h4>Календарь аттестаций</h4>
                                    </div>
                                </div>
                                <div class=panel-body>
                                    <div class=monthly_calender>
                                        <div class=monthly id=m_calendar></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-8">
                            <div class="panel lobidisable panel-bd">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Компетенции</h4>
                                    </div>
                                </div>
                                <?php
                                    $items = array(
                                        "Основы программирования: типы и структуры данных",
                                        "Паттерны проектирования и основные подходы к архитектуре систем",
                                        "Теория алгоритмов",
                                        "Теория машинного обучения",
                                        "Теория облачных технологий",
                                        "Теория Blockchain/ распределенный реестр",
                                        "Теория защиты информации",
                                        "Криптография",
                                        "Java",
                                        "C++",
                                        "C#",
                                        "SQL",
                                        "Python",
                                        "Scala",
                                        "R",
                                        "Go (Golang)",
                                        "Ruby",
                                        ".Net",
                                        "1С",
                                        "PHP",
                                        "JavaScript",
                                        "HTML CSS",
                                        "Swift",
                                        "Visual Basic",
                                        "Django",
                                        "jQuery",
                                        "React",
                                        "Мобильная разработка iOs",
                                        "Мобильная разработка Android",
                                        "Unity 3D",
                                        "Unreal Engine",
                                        "Разработка UX/UI",
                                        "Проектирование и дизайн мобильных интерфейсов",
                                        "Проектирование и дизайн веб-интерфейсов",
                                        "Инфографика",
                                        "Adobe Photoshop/ Adobe Illustrator",
                                        "Sketch",
                                        "Figma",
                                        "Методы моделирования и аналитика бизнес-процессов",
                                        "Product Management",
                                        "Project Management",
                                        "Гибкие методологии управления проектами",
                                        "Юридическая и нормативная база в ИТ",
                                        "Финансовое планирование",
                                        "Управление инвестициями",
                                        "Управления финансовыми рисками",
                                        "Бренд-стратегия",
                                        "Контекстная реклама",
                                        "Мобильный маркетинг",
                                        "Трейд-маркетинг"
                                    );
                                ?>
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="dataTableExampleUser" class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Компетенции</th>
                                                    <th>Оценка</th>
                                                    <th>Дата аттестации</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ( $items as $key => $item ) : ?>
                                                    <tr>
                                                        <th scope="row"><?php echo $key + 1; ?></th>
                                                        <td><?php echo $item; ?></td>
                                                        <td><?php echo rand( 1, 20 ); ?></td>
                                                        <td>01.06.2019</td>
                                                    </tr>
                                                <?php endforeach; ?>                                                                       
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>                                               
                    </div>
                </div>
            </div>
            
<?php require_once( 'inc/footer.php' ); ?>