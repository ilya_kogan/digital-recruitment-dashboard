<?php require_once( 'inc/header.php' ); ?>
   
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                <div class="content">
                    <!-- Content Header (Page header) -->
                    <div class="content-header">
                        <div class="header-icon">
                            <i class="pe-7s-graph3"></i>
                        </div>
                        <div class="header-title">
                            <h1>Дашборд</h1>
                            <small>Образовательного учреждения</small>
                        </div>
                    </div> <!-- /. Content Header (Page header) -->

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Список студентов</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <div class="table-responsive">                                        
                                        <table id="dataTableExample1" class="table table-bordered table-striped table-hover">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Фамилия</th>
                                                    <th>Специальность/Факультет</th>
                                                    <th>Возраст</th>
                                                    <th>Выпускается</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ( $users as $key => $user ) : ?>
                                                    <tr>
                                                        <td><?php echo $key + 1; ?></td>
                                                        <td><a href="/profile.php?role=<?php echo $_GET['role']; ?>&first_name=<?php echo $user[0]; ?>&last_name=<?php echo $user[1]; ?>"><?php echo $user[0]; ?></a></td>
                                                        <td><a href="/profile.php?role=<?php echo $_GET['role']; ?>&first_name=<?php echo $user[0]; ?>&last_name=<?php echo $user[1]; ?>"><?php echo $user[1]; ?></a></td>
                                                        <td><?php echo $user[2]; ?></td>
                                                        <td><?php echo $user[3]; ?></td>
                                                        <td><?php echo $user[4]; ?></td>
                                                    </tr>
                                                <?php endforeach; ?>                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-lg-4">
                            <div class="panel panel-bd lobidisable">
                                <div class=panel-heading>
                                    <div class=panel-title>
                                        <i class=ti-email></i>
                                        <h4>Лидеры по компетенциям</h4>
                                    </div>
                                </div>

                                <div class=panel-body>
                                    <div class=message_inner>
                                        <div class=message_widgets>
                                            <?php foreach ( $users as $key => $user ) : ?>
                                                <a href="#">
                                                    <div class=inbox-item>
                                                        <div class=inbox-item-img><img src="../assets/dist/img/avatar<?php echo rand(2, 5); ?>.png" class=img-circle alt=""></div>
                                                         <strong class=inbox-item-author><?php echo $user[0] . ' ' . $user[1]; ?> (ФГО/ИСТ-131)</strong>
                                                        <span class=inbox-item-date>-13:40 PM</span>
                                                        <p class=inbox-item-text>PHP, C++, HTML</p>
                                                        <span class="profile-status available pull-right"></span>
                                                    </div>
                                                </a>
                                            <?php endforeach; ?>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 hidden-sm hidden-xs">
                            <div class="panel panel-bd lobidisable lg-mb0">
                                <div class=panel-heading>
                                    <div class=panel-title>
                                        <i class=ti-archive></i>
                                        <h4>Календарь аттестаций </h4>
                                    </div>
                                </div>
                                <div class=panel-body>
                                    <div class=monthly_calender>
                                        <div class=monthly id=m_calendar></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-6 hidden-xs">
                            <div class="panel panel-bd lobidisable lg-mb0">
                                <div class=panel-heading>
                                    <div class=panel-title>
                                        <i class=ti-stats-up></i>
                                        <h4>Последние активности
                                        </h4>
                                    </div>
                                </div>
                                <div class=panel-body>
                                    <ul class="activity-list list-unstyled">
                                        <li class=activity-purple>
                                            <small class=text-muted>9 минут назад</small>
                                            <p><span class="label label-success label-pill">Обновились</span> данные по компетенциям</p>
                                        </li>
                                        <li class=activity-danger>
                                            <small class=text-muted>15 минут назад</small>
                                            <p>Вам ответил Геннадий Петров</p>
                                        </li>
                                        <li class=activity-warning>
                                            <small class=text-muted>22 минут назад</small>
                                            <p><span class="label label-success label-pill">Обновились</span> данные по университетам</p>
                                        </li>
                                        <li class=activity-primary>
                                            <small class=text-muted>30 минут назад</small>
                                            <p>Вы изменили данные в профиле</p>
                                        </li>
                                        <li>
                                            <small class=text-muted>35 минут назад</small>
                                            <p>Вы обновили пароль</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <!-- Bar Chart -->
                        <div class="col-sm-6 col-md-6">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Уровень компетенций сводная</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="barChartDesign" height="200"></canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="panel panel-bd lobidrag">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4>Уровень компетенций по курсам</h4>
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <canvas id="barChartPM" height="200"></canvas>
                                </div>
                            </div>
                        </div>                                                                                               
                    </div>
                </div>
            </div>
            
<?php require_once( 'inc/footer.php' ); ?>