$(document).ready(function () {
    //bar chart 2
    var ctxDesign_1 = document.getElementById("barChartDesign");
    var myChart = new Chart(ctxDesign_1, {
        type: 'bar',
        data: {
            labels: ["Разработка UX/UI", "Проектирование и дизайн мобильных интерфейсов", "Проектирование и дизайн веб-интерфейсов", "Инфографика", "Adobe Photoshop/ Adobe Illustrator", "Sketch", "Figma"],
            datasets: [
                {
                    label: "01.01.2019",
                    data: [25, 67, 25, 67, 98, 46, 87],
                    borderColor: "rgba(87,120,123,0.6)",
                    borderWidth: "0",
                    backgroundColor: "rgba(87,120,123,0.4)"
                },
                {
                    label: "01.06.2019",
                    data: [55, 89, 47, 89, 99, 67, 98],
                    borderColor: "rgba(87,120,123,0.7)",
                    borderWidth: "0",
                    backgroundColor: "rgba(87,120,123,0.5)"
                }
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        display: false,
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    //bar chart 3
    var ctxPM_1 = document.getElementById("barChartPM");
    var myChart = new Chart(ctxPM_1, {
        type: 'bar',
        data: {
            labels: ["Методы моделирования и аналитика бизнес-процессов", "Product Management", "Project Management", "Гибкие методологии управления проектами", "Юридическая и нормативная база в ИТ", "Финансовое планирование", "Управление инвестициями", "Управления финансовыми рисками", "Бренд-стратегия", "Контекстная реклама", "Мобильный маркетинг", "Трейд-маркетинг"],
            datasets: [
                {
                    label: "1 курс",
                    data: [67, 98, 46, 87, 25, 67, 25, 78, 54, 43, 77, 86],
                    borderColor: "rgba(87,120,123,0.6)",
                    borderWidth: "0",
                    backgroundColor: "rgba(87,120,123,0.4)"
                },
                {
                    label: "2 курс",
                    data: [78, 88, 87, 97, 56, 88, 54, 88, 78, 78, 89, 94],
                    borderColor: "rgba(87,120,123,0.7)",
                    borderWidth: "0",
                    backgroundColor: "rgba(87,120,123,0.5)"
                }
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        display: false,
                        beginAtZero: true
                    }
                }]
            }
        }
    });    
});