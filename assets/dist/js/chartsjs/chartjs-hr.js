$(document).ready(function () {
    //Sparklines Charts
    $('.sparkline1').sparkline([4, 6, 7, 7, 4, 3, 2, 4, 6, 7, 4, 6, 7, 7, 4, 3, 2, 4, 6, 7, 7, 4, 3, 1, 5, 7, 6, 6, 5, 5, 4, 4, 3, 3, 4, 4, 5, 6, 7, 2, 3, 4], {
        type: 'bar',
        barColor: '#fff',
        height: '40',
        barWidth: '3',
        barSpacing: 2
    });

    $(".sparkline2").sparkline([4, 6, 7, 7, 4, 3, 2, 1, 4, 4, 5, 6, 3, 4, 5, 8, 7, 6, 9, 3, 2, 4, 1, 5, 6, 4, 3, 7, 6, 8, 3, 2, 6], {
        type: 'discrete',
        lineColor: '#fff',
        width: '200',
        height: '40'
    });

    $(".sparkline3").sparkline([5, 6, 7, 2, 0, -4, -2, -3, -4, 4, 5, 6, 3, 2, 4, -6, -5, -4, 6, 5, 4, 3, 4, -3, -5, -4, 5, 4, 3, 6, -2, -3, -4, -5, 5, 6, 3, 4, 5], {
        type: 'bar',
        barColor: '#fff',
        negBarColor: '#c6c6c6',
        width: '200',
        height: '40'
    });

    $(".sparkline4").sparkline([10, 34, 13, 33, 35, 24, 32, 24, 52, 35], {
        type: 'line',
        height: '40',
        width: '100%',
        lineColor: '#fff',
        fillColor: 'rgba(255,255,255,0.1)'
    });

	
    //polar chart 1
    var ctx = document.getElementById("polarChart");
    var myChart = new Chart(ctx, {
        type: 'polarArea',
        data: {
            datasets: [{
                data: [56, 33, 32, 64, 27, 19, 86, 84, 32, 46, 25, 22, 25, 67, 98, 46, 87, 78, 76, 69, 74, 35, 73, 67, 46, 54, 84, 74, 36, 68, 98],
                backgroundColor: [
                    "rgba(90,120,123,0.9)",
                    "rgba(80,10,123,0.8)",
                    "rgba(70,220,123,0.7)",
                    "rgba(60,0,0,0.2)",
                    "rgba(87,120,44,0.5)",
                    "rgba(17,55,123,0.5)",
                    "rgba(87,180,102,0.5)",
                    "rgba(67,120,123,0.5)",
                    "rgba(87,33,245,0.5)",
                    "rgba(134,20,2,0.5)",
                    "rgba(87,245,13,0.5)",
                    "rgba(87,120,55,0.5)",
                    "rgba(87,10,123,0.5)",
                    "rgba(7,120,36,0.5)",
                    "rgba(56,245,123,0.5)",
                    "rgba(87,10,113,0.5)",
                    "rgba(11,32,123,0.5)",
                    "rgba(87,124,223,0.5)",
                    "rgba(2,12,11,0.5)",
                    "rgba(87,22,312,0.5)",
                    "rgba(33,120,123,0.5)",
                    "rgba(56,32,123,0.5)",
                    "rgba(223,124,223,0.5)",
                    "rgba(123,12,11,0.5)",
                    "rgba(99,22,312,0.5)",
                    "rgba(34,22,312,0.5)",
                    "rgba(177,22,312,0.5)",
                    "rgba(45,22,312,0.5)",
                    "rgba(135,22,312,0.5)",
                    "rgba(54,22,312,0.5)",
                    "rgba(98,22,312,0.5)"
                ]
            }],
            labels: ["Основы программирования: типы и структуры данных", "Паттерны проектир. и основные. подходы к арх. систем.", "Теория алгоритмов", "Теория машинного обучения", "Теория облачных технологий", "Теория Blockchain/распределенный реестр", "Теория защиты информации", "Криптография", "Java", "C++", "C#", "SQL", "Python", "Scala", "R", "Go (Golang)", "Ruby", ".Net", "1С", "PHP", "JavaScript", "HTML CSS", "Swift", "Visual Basic", "Django", "jQuery", "React", "Мобильная разработка iOs", "Мобильная разработка Android", "Unity 3D", "Unreal Engine"]
        },
        options: {
            legend: {
                display: false
            },
            responsive: true
        }
    });

    //polar chart 2
    var ctx = document.getElementById("polarChart_2");
    var myChart = new Chart(ctx, {
        type: 'polarArea',
        data: {
            datasets: [{
                data: [25, 67, 25, 67, 98, 46, 87],
                backgroundColor: [
                    "rgba(60,0,0,0.2)",
                    "rgba(87,120,44,0.5)",
                    "rgba(17,55,123,0.5)",
                    "rgba(0,0,0,0.2)",
                    "rgba(67,120,123,0.5)",
                    "rgba(87,33,245,0.5)",
                    "rgba(17, 20, 123, 0.5)"
                ]
            }],
            labels: ["Разработка UX/UI", "Проектирование и дизайн мобильных интерфейсов", "Проектирование и дизайн веб-интерфейсов", "Инфографика", "Adobe Photoshop/ Adobe Illustrator", "Sketch", "Figma"]
        },
        options: {
            legend: {
                display: false
            },
            responsive: true
        }
    });

    //polar chart 3
    var ctx = document.getElementById("polarChart_3");
    var myChart = new Chart(ctx, {
        type: 'polarArea',
        data: {
            datasets: [{
                data: [67, 98, 46, 87, 25, 67, 25, 78, 54, 43, 77, 86],
                backgroundColor: [
                    "rgba(90,120,123,0.9)",
                    "rgba(80,10,123,0.8)",
                    "rgba(70,220,123,0.7)",
                    "rgba(60,0,0,0.2)",
                    "rgba(87,120,44,0.5)",
                    "rgba(17,55,123,0.5)",
                    "rgba(87,180,102,0.5)",
                    "rgba(67,120,123,0.5)",
                    "rgba(60,0,70,0.2)",
                    "rgba(87,12,44,0.5)",
                    "rgba(17,55,12,0.5)"
                ]
            }],            
            labels: ["Методы моделирования и аналитика бизнес-процессов", "Product Management", "Project Management", "Гибкие методологии управления проектами", "Юридическая и нормативная база в ИТ", "Финансовое планирование", "Управление инвестициями", "Управления финансовыми рисками", "Бренд-стратегия", "Контекстная реклама", "Мобильный маркетинг", "Трейд-маркетинг"]
        },
        options: {
            legend: {
                display: false
            },
            responsive: true
        }
    });
});