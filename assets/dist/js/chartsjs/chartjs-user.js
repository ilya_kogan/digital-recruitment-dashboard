$(document).ready(function () {
    //Sparklines Charts
    $('.sparkline1').sparkline([4, 6, 7, 7, 4, 3, 2, 4, 6, 7, 4, 6, 7, 7, 4, 3, 2, 4, 6, 7, 7, 4, 3, 1, 5, 7, 6, 6, 5, 5, 4, 4, 3, 3, 4, 4, 5, 6, 7, 2, 3, 4], {
        type: 'bar',
        barColor: '#fff',
        height: '40',
        barWidth: '3',
        barSpacing: 2
    });

    $(".sparkline2").sparkline([4, 6, 7, 7, 4, 3, 2, 1, 4, 4, 5, 6, 3, 4, 5, 8, 7, 6, 9, 3, 2, 4, 1, 5, 6, 4, 3, 7, 6, 8, 3, 2, 6], {
        type: 'discrete',
        lineColor: '#fff',
        width: '200',
        height: '40'
    });

    $(".sparkline3").sparkline([5, 6, 7, 2, 0, -4, -2, -3, -4, 4, 5, 6, 3, 2, 4, -6, -5, -4, 6, 5, 4, 3, 4, -3, -5, -4, 5, 4, 3, 6, -2, -3, -4, -5, 5, 6, 3, 4, 5], {
        type: 'bar',
        barColor: '#fff',
        negBarColor: '#c6c6c6',
        width: '200',
        height: '40'
    });

    $(".sparkline4").sparkline([10, 34, 13, 33, 35, 24, 32, 24, 52, 35], {
        type: 'line',
        height: '40',
        width: '100%',
        lineColor: '#fff',
        fillColor: 'rgba(255,255,255,0.1)'
    });
    
    //bar chart 1
    var ctxIT_1 = document.getElementById("barChartIT");
    var myChart = new Chart(ctxIT_1, {
        type: 'bar',
        data: {
            labels: ["Основы программирования: типы и структуры данных", "Паттерны проектирования и основные подходы к архитектуре систем", "Теория алгоритмов", "Теория машинного обучения", "Теория облачных технологий", "Теория Blockchain/ распределенный реестр", "Теория защиты информации", "Криптография", "Java", "C++", "C#", "SQL", "Python", "Scala", "R", "Go (Golang)", "Ruby", ".Net", "1С", "PHP", "JavaScript", "HTML CSS", "Swift", "Visual Basic", "Django", "jQuery", "React", "Мобильная разработка iOs", "Мобильная разработка Android", "Unity 3D", "Unreal Engine"],
            datasets: [
                {
                    label: "01.01.2019",
                    data: [56, 33, 32, 64, 27, 19, 86, 84, 32, 46, 25, 22, 25, 67, 98, 46, 87, 78, 76, 69, 74, 35, 73, 67, 46, 54, 84, 74, 36, 68, 98],
                    borderColor: "rgba(87,120,123,0.9)",
                    borderWidth: "0",
                    backgroundColor: "rgba(87,120,123,0.5)" 
                },
                {
                    label: "01.06.2019",
                    data: [88, 77, 43, 88, 56, 44, 93, 87, 45, 78, 34, 54, 56, 76, 99, 87, 88, 90, 87, 54, 88, 45, 78, 78, 67, 87, 88, 74, 46, 78, 98],
                    borderColor: "rgba(0,0,0,0.09)",
                    borderWidth: "0",
                    backgroundColor: "rgba(0,0,0,0.07)"
                }
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        display: false,
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    //bar chart 2
    var ctxDesign_1 = document.getElementById("barChartDesign");
    var myChart = new Chart(ctxDesign_1, {
        type: 'bar',
        data: {
            labels: ["Разработка UX/UI", "Проектирование и дизайн мобильных интерфейсов", "Проектирование и дизайн веб-интерфейсов", "Инфографика", "Adobe Photoshop/ Adobe Illustrator", "Sketch", "Figma"],
            datasets: [
                {
                    label: "01.01.2019",
                    data: [25, 67, 25, 67, 98, 46, 87],
                    borderColor: "rgba(87, 120, 123, 0.6)",
                    borderWidth: "0",
                    backgroundColor: "rgba(87, 120, 123, 0.4)"
                },
                {
                    label: "01.06.2019",
                    data: [55, 89, 47, 89, 99, 67, 98],
                    borderColor: "rgba(87, 120, 123, 0.7",
                    borderWidth: "0",
                    backgroundColor: "rgba(87, 120, 123, 0.5)"
                }
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        display: false,
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    //bar chart 3
    var ctxPM_1 = document.getElementById("barChartPM");
    var myChart = new Chart(ctxPM_1, {
        type: 'bar',
        data: {
            labels: ["Методы моделирования и аналитика бизнес-процессов", "Product Management", "Project Management", "Гибкие методологии управления проектами", "Юридическая и нормативная база в ИТ", "Финансовое планирование", "Управление инвестициями", "Управления финансовыми рисками", "Бренд-стратегия", "Контекстная реклама", "Мобильный маркетинг", "Трейд-маркетинг"],
            datasets: [
                {
                    label: "01.01.2019",
                    data: [67, 98, 46, 87, 25, 67, 25, 78, 54, 43, 77, 86],
                    borderColor: "rgba(87, 120, 123, 0.6)",
                    borderWidth: "0",
                    backgroundColor: "rgba(87, 120, 123, 0.4)"
                },
                {
                    label: "01.06.2019",
                    data: [78, 88, 87, 97, 56, 88, 54, 88, 78, 78, 89, 94],
                    borderColor: "rgba(87, 120, 123, 0.7",
                    borderWidth: "0",
                    backgroundColor: "rgba(87, 120, 123, 0.5)"
                }
            ]
        },
        options: {
            scales: {
                xAxes: [{
                    ticks: {
                        display: false,
                        beginAtZero: true
                    }
                }]
            }
        }
    });

    //radar chart 1
    var ctxIT_2 = document.getElementById("radarChartIT");
    var myChart = new Chart(ctxIT_2, {
        type: 'radar',
        data: {
            labels: ["Основы программирования: типы и структуры данных", "Паттерны проектирования и основные подходы к архитектуре систем", "Теория алгоритмов", "Теория машинного обучения", "Теория облачных технологий", "Теория Blockchain/ распределенный реестр", "Теория защиты информации", "Криптография", "Java", "C++", "C#", "SQL", "Python", "Scala", "R", "Go (Golang)", "Ruby", ".Net", "1С", "PHP", "JavaScript", "HTML CSS", "Swift", "Visual Basic", "Django", "jQuery", "React", "Мобильная разработка iOs", "Мобильная разработка Android", "Unity 3D", "Unreal Engine"],
            datasets: [
                {
                    label: "01.01.2019",
                    data: [56, 33, 32, 64, 27, 19, 86, 84, 32, 46, 25, 22, 25, 67, 98, 46, 87, 78, 76, 69, 74, 35, 73, 67, 46, 54, 84, 74, 36, 68, 98],
                    borderColor: "rgba(87, 120, 123, 0.6)",
                    borderWidth: "1",
                    backgroundColor: "rgba(87, 120, 123, 0.4)"
                },
                {
                    label: "01.06.2019",
                    data: [88, 77, 43, 88, 56, 44, 93, 87, 45, 78, 34, 54, 56, 76, 99, 87, 88, 90, 87, 54, 88, 45, 78, 78, 67, 87, 88, 74, 46, 78, 98],
                    borderColor: "rgba(87, 120, 123, 0.7",
                    borderWidth: "1",
                    backgroundColor: "rgba(87, 120, 123, 0.5)"
                }
            ]            
        },        
        options: {
            title: {
                display: true,
                text: 'Информационные технологии'
            },
            legend: {
                position: 'top',
                display: true,
                labels: {
                    fontColor: '#000'
                }
            },
            scale: {
                ticks: {
                    beginAtZero: true
                },
                pointLabels: {
                    callback: function(pointLabel, index, labels) {
                        return '';
                    }
                }
            }
        }
    });

    //radar chart 2
    var ctxDesign_2 = document.getElementById("radarChartDesign");
    var myChart = new Chart(ctxDesign_2, {
        type: 'radar',
        data: {
            labels: ["Разработка UX/UI", "Проектирование и дизайн мобильных интерфейсов", "Проектирование и дизайн веб-интерфейсов", "Инфографика", "Adobe Photoshop/ Adobe Illustrator", "Sketch", "Figma"],
            datasets: [
                {
                    label: "01.01.2019",
                    data: [25, 67, 25, 67, 98, 46, 87],
                    borderColor: "rgba(87, 120, 123, 0.6)",
                    borderWidth: "1",
                    backgroundColor: "rgba(87, 120, 123, 0.4)"
                },
                {
                    label: "01.06.2019",
                    data: [55, 89, 47, 89, 99, 67, 98],
                    borderColor: "rgba(87, 120, 123, 0.7",
                    borderWidth: "1",
                    backgroundColor: "rgba(87, 120, 123, 0.5)"
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'Дизайн'
            },
            legend: {
                position: 'top',
                display: true,
                labels: {
                    fontColor: '#000'
                }
            },
            scale: {
                ticks: {
                    beginAtZero: true
                },
                pointLabels: {
                    callback: function(pointLabel, index, labels) {
                        return '';
                    } 
                }
            }
        }
    });

    // //radar chart 3
    var ctxPM_2 = document.getElementById("radarChartPM");
    var myChart = new Chart(ctxPM_2, {
        type: 'radar',
        data: {
            labels: ["Методы моделирования и аналитика бизнес-процессов", "Product Management", "Project Management", "Гибкие методологии управления проектами", "Юридическая и нормативная база в ИТ", "Финансовое планирование", "Управление инвестициями", "Управления финансовыми рисками", "Бренд-стратегия", "Контекстная реклама", "Мобильный маркетинг", "Трейд-маркетинг"],
            datasets: [
                {
                    label: "01.01.2019",
                    data: [67, 98, 46, 87, 25, 67, 25, 78, 54, 43, 77, 86],
                    borderColor: "rgba(87, 120, 123, 0.6)",
                    borderWidth: "1",
                    backgroundColor: "rgba(87, 120, 123, 0.4)"
                },
                {
                    label: "01.06.2019",
                    data: [78, 88, 87, 97, 56, 88, 54, 88, 78, 78, 89, 94],
                    borderColor: "rgba(87, 120, 123, 0.7",
                    borderWidth: "1",
                    backgroundColor: "rgba(87, 120, 123, 0.5)"
                }
            ]
        },
        options: {
            title: {
                display: true,
                text: 'Управление проектами и бизнес-анализ'
            },
            legend: {
                position: 'top',
                display: true,
                labels: {
                    fontColor: '#000'
                }
            },
            scale: {
                ticks: {
                    beginAtZero: true
                },
                pointLabels: {
                    callback: function(pointLabel, index, labels) {
                        return '';
                    } 
                }
            }
        }
    });

    //Table
    $('#dataTableExampleUser').DataTable({
        dom: "<'row dataTableInner'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp",
        "lengthMenu": [[5, 25, 50, -1], [5, 25, 50, "All"]],
        "searching": false,
        "lengthChange": false,
        buttons: [
            {extend: 'copy', className: 'btn-sm'},
            {extend: 'csv', title: 'ExampleFile', className: 'btn-sm'},
            {extend: 'excel', title: 'ExampleFile', className: 'btn-sm'},
            {extend: 'pdf', title: 'ExampleFile', className: 'btn-sm'},
            {extend: 'print', className: 'btn-sm'}
        ]
    });
});