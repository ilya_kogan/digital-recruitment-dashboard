			<footer class="main-footer">
                <strong>Copyright &copy; 2019</strong>
            </footer>
        </div> <!-- ./wrapper -->
        
        <!-- START CORE PLUGINS -->
        <script src="../assets/plugins/jQuery/jquery-1.12.4.min.js"></script>
        <script src="../assets/plugins/jquery-ui-1.12.1/jquery-ui.min.js"></script>
        <script src="../assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="../assets/plugins/metisMenu/metisMenu.min.js"></script>
        <script src="../assets/plugins/lobipanel/lobipanel.min.js"></script>
        <script src="../assets/plugins/fastclick/fastclick.min.js"></script>
        <script src="../assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- sparkline Js -->
        <script src="../assets/plugins/sparkline/sparkline.min.js"></script>
        <!-- counterup Js -->
        <script src="../assets/plugins/counterup/jquery.counterup.min.js"></script>
        <script src="../assets/plugins/counterup/waypoints.js"></script>
        <!-- emojionearea Js -->
        <script src="../assets/plugins/emojionearea/emojionearea.min.js"></script>
        <script src="../assets/plugins/monthly/monthly.min.js"></script>
        <!-- amcharts Js -->
        <script src="../assets/plugins/amcharts/amcharts.js"></script>
        <script src="../assets/plugins/amcharts/ammap.js"></script>
        <script src="../assets/plugins/amcharts/worldLow.js"></script>
        <script src="../assets/plugins/amcharts/serial.js"></script>
        <script src="../assets/plugins/amcharts/export.min.js"></script>
        <script src="../assets/plugins/amcharts/light.js"></script>
        <script src="../assets/plugins/amcharts/pie.js"></script>
        <!-- START THEME LABEL SCRIPT -->
        <script src="../assets/dist/js/page/dashboard.js"></script>
        <script src="../assets/dist/js/theme.js"></script>

        <?php if ( isset( $_GET['role'] ) && ! empty( $_GET['role'] ) ) : ?>
            <!-- Chart Js -->
            <script src="../assets/plugins/chartJs/Chart.min.js"></script>
            <script src="../assets/dist/js/chartsjs/chartjs-<?php echo $_GET['role']; ?>.js"></script>

            <?php if ( $_GET['role'] == 'gos' || $_GET['role'] == 'hr' ) : ?>
                <!-- amcharts js -->
                <script src="../assets/plugins/amcharts/ammap.js"></script>
                <script src="../assets/plugins/amcharts/worldLow.js"></script>
                <script src="../assets/plugins/amcharts/export.min.js"></script>
                <script src="../assets/plugins/amcharts/light.js"></script>
                <script src="../assets/plugins/amcharts/usaLow.js"></script>
                <script src="../assets/dist/js/maps/amcharts-map.js"></script>
            <?php endif; ?>

            <?php if ( $_GET['role'] == 'user' || $_GET['role'] == 'hr' || $_GET['role'] == 'edu' ) : ?>
                <!-- DataTable Js -->
                <script src="../assets/plugins/datatables/dataTables.min.js"></script>
                <script src="../assets/plugins/datatables/dataTables-active.js"></script>
            <?php endif; ?>
        <?php endif; ?>
    </body>
</html>